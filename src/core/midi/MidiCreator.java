package core.midi;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiDevice.Info;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Soundbank;
import javax.sound.midi.Synthesizer;
import javax.sound.midi.Track;

import com.sun.media.sound.AudioSynthesizer;

import core.midi.MidiCreator.MidiTone;
import core.tones.FrequenceFinder;
import core.tones.ToneData;

public class MidiCreator extends ArrayList<MidiTone>
{
   /**
    *
    */
   private static final long serialVersionUID = 1123498006838403839L;

   // Inner class
   public static class MidiTone
   {
      private static int getNoteIndex(String note)
      {
         if (MAP.get(note) == null)
         {
            return -999;
         }
         else
         {
            return MAP.get(note).intValue();
         }
      }

      public static boolean isNumeric(String numberString)
      {
         try
         {
            Integer.parseInt(numberString);
            return true;
         }
         catch (NumberFormatException e)
         {
            return false;
         }
      }

      public static String transformMidiToneIndexToName(int midiToneIndex)
      {
         if (midiToneIndex < 0)
         {
            return "??? " + midiToneIndex;
         }

         int octave = midiToneIndex / 12 - 1;
         int index = midiToneIndex % 12;
         return FrequenceFinder.getToneName(index) + String.valueOf(octave);
      }

      public static int transformNoteNameToMidiToneIndex(String noteName)
      {
         if (noteName == null)
         {
            return -1;
         }

         String octaveString =
               noteName.substring(noteName.length() - 1, noteName.length());
         int octave = 0;
         String note;
         if (isNumeric(octaveString))
         {
            octave = Integer.parseInt(octaveString);
            note = noteName.substring(0, noteName.length() - 1);
         }
         else
         {
            note = noteName;
         }

         int noteIndex = getNoteIndex(note);

         return noteIndex + (octave + 1) * 12;
      }

      private final double iDurationInMilliSec;
      private int iMidiToneIndex;

      private final String iNoteName;

      private int iTicks;

      private ToneData iToneData = null;

      private final int iVelocity;

      public MidiTone(double durationInMilliSec)
      {
         iDurationInMilliSec = durationInMilliSec;
         iTicks = transformMilliSecToTicks(iDurationInMilliSec);
         if (iTicks < 0)
         {
            System.out.println("Bad Ticks: " + iTicks);
            iTicks = 0;
         }

         iVelocity = -1;
         iNoteName = null;

      }

      public MidiTone(String noteName, double durationInMilliSec, int velocity)
      {

         iDurationInMilliSec = durationInMilliSec;
         iTicks = transformMilliSecToTicks(iDurationInMilliSec);
         iVelocity = velocity;
         iNoteName = noteName;
         if (noteName == null)
         {
            iMidiToneIndex = -1;
         }
         else
         {
            iMidiToneIndex = transformNoteNameToMidiToneIndex(noteName);
            if (iMidiToneIndex < 0 || iMidiToneIndex > 127)
            {
               System.out.println("Bad Midi tone: " + iMidiToneIndex);
               iMidiToneIndex = 0;
            }

         }
      }

      public MidiTone(ToneData toneData)
      {
         this(toneData.getNote().getName(),
               toneData.getNoteLength().getMultiple(), toneData.getVelocity());
         iToneData = toneData;
      }

      public double getDurationInMilliSec()
      {
         return iDurationInMilliSec;
      }

      public int getMidiToneIndex()
      {
         return iMidiToneIndex;
      }

      public String getNoteName()
      {
         return iNoteName;
      }

      public int getTicks()
      {
         return iTicks;
      }

      public int getVelocity()
      {
         return iVelocity;
      }

      public void setPlayedNoteIndex(int index)
      {
         if (iToneData != null)
         {
            iToneData.setPlayedNoteIndex(index);
         }
      }

      public void setStartTick(int startTick)
      {
         if (iToneData != null)
         {
            iToneData.setStartTick(startTick);
         }
      }

      public void setTickLength(int tickLength)
      {
         if (iToneData != null)
         {
            iToneData.setTickLength(tickLength);
         }
      }

   }

   private static final float CONSTANT_SILENSE = 20;
   private static final Map<String, Integer> MAP = setNoteIndexes();

   private static AudioSynthesizer getAudioSynthesizer()
   {
      try
      {
         // First check if default synthesizer is AudioSynthesizer.
         Synthesizer synth = MidiSystem.getSynthesizer();
         if (synth instanceof AudioSynthesizer)
         {
            return (AudioSynthesizer) synth;
         }

         // If default synhtesizer is not AudioSynthesizer, check others.
         Info[] infos = MidiSystem.getMidiDeviceInfo();
         for (int i = 0; i < infos.length; i++)
         {
            MidiDevice dev = MidiSystem.getMidiDevice(infos[i]);
            if (dev instanceof AudioSynthesizer)
            {
               return (AudioSynthesizer) dev;
            }
         }

      }
      catch (MidiUnavailableException e1)
      {
         e1.printStackTrace();
      }
      return null;
   }

   //
   public static Soundbank getSoundBank()
   {
      AudioSynthesizer audio;
      audio = getAudioSynthesizer();
      Soundbank soundbank = audio.getDefaultSoundbank();
      return soundbank;
   }

   private static Map<String, Integer> setNoteIndexes()
   {
      Map<String, Integer> map2 = new HashMap<String, Integer>();
      String[] notes = FrequenceFinder.getToneNames();
      int i = 0;
      for (String tone : notes)
      {
         map2.put(tone, new Integer(i));
         i++;
      }

      return map2;
   }

   private static int transformMilliSecToTicks(double milliSec)
   {
      return (int) Math.round(milliSec / 33);
   }

   private final String iFilename;

   private Sequence sequence;

   public MidiCreator(String filename)
   {
      iFilename = filename;
   }

   public MidiCreator(String filename, List<ToneData> lst)
   {
      iFilename = filename;
      if (lst.size() > 0)
      {

         for (ToneData toneData : lst)
         {
            if (toneData.isTone())
            {
               addNote(toneData);
            }
            else
            {
               addSilens(toneData.getDurationInMilliSeconds());
            }
            addSilens(CONSTANT_SILENSE);
         }
      }
   }

   private void addNote(ToneData toneData)
   {
      add(new MidiTone(toneData));
   }

   private void addSilens(double durationInMilliSec)
   {
      add(new MidiTone(durationInMilliSec));
   }

   private void createFile() throws IOException
   {

      // A file name was specified, so save the notes
      int[] allowedTypes = MidiSystem.getMidiFileTypes(sequence);
      if (allowedTypes.length == 0)
      {
         System.err.println("No supported MIDI file types.");
      }
      else
      {
         MidiSystem.write(sequence, allowedTypes[0], new File(iFilename));
      }
   }

   public void createMidiFile(int instrument, boolean makeEvent)
   {
      try
      {
         sequence = new Sequence(Sequence.PPQ, 16);
         Track track = sequence.createTrack(); // Begin with a new track

         // Set the instrument on channel 0
         ShortMessage sm = new ShortMessage();
         sm.setMessage(ShortMessage.PROGRAM_CHANGE, 0, instrument, 0);
         track.add(new MidiEvent(sm, 0));

         int startTick = 0;
         int playedNoteIndex = 0;

         // Create midifile
         boolean firstNote = true;
         for (MidiTone wt : this)
         {
            int tickLength = wt.getTicks();
            if (wt.getMidiToneIndex() > 0)
            {
               ShortMessage on = new ShortMessage();
               on.setMessage(ShortMessage.NOTE_ON, 0, wt.getMidiToneIndex(),
                     wt.getVelocity());
               ShortMessage off = new ShortMessage();
               off.setMessage(ShortMessage.NOTE_OFF, 0, wt.getMidiToneIndex(),
                     wt.getVelocity());
               System.out.println("Key: " + wt.getMidiToneIndex()
                     + ", Velocity:" + wt.getVelocity() + ", StartTick: "
                     + startTick + ", TickLength: " + tickLength
                     + ", PlayedNoteIndex: " + playedNoteIndex);
               wt.setPlayedNoteIndex(playedNoteIndex);
               playedNoteIndex++;
               if (makeEvent)
               {
                  track.add(makeEvent(ShortMessage.CONTROL_CHANGE, 1, 127, 1,
                        startTick));
               }
               track.add(new MidiEvent(on, startTick));
               wt.setStartTick(startTick);
               if (makeEvent)
               {
                  track.add(makeEvent(ShortMessage.CONTROL_CHANGE, 0, 127, 0,
                        startTick + tickLength));
               }
               wt.setTickLength(tickLength);
               track.add(new MidiEvent(off, startTick + tickLength));
            }
            if (wt.getMidiToneIndex() > 0 || !firstNote)
            {
               // Avoid starting with a silent note
               startTick += tickLength;
            }
            firstNote = false;
         }

         // save midifile
         createFile();
      }
      catch (InvalidMidiDataException e)
      {
         e.printStackTrace();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
   }

   public static MidiEvent makeEvent(int comd, int chan, int one, int two,
         int tick)
   {
      MidiEvent event = null;
      try
      {
         ShortMessage a = new ShortMessage();
         a.setMessage(comd, chan, one, two);
         event = new MidiEvent(a, tick);
      } // close try
      catch (Exception e)
      {
      } // close catch

      return event;
   } // close makeEvent method

}
