package core.midi;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.sound.midi.ControllerEventListener;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;

public class MidiPlayer extends Thread
{
   private String iFileName;
   private float iTempo;
   private Sequence sequence;
   private Sequencer sequencer;

   public MidiPlayer(String midiFileName, int tempo)
   {
      iFileName = midiFileName;
      iTempo = tempo;
      try
      {
         // Create a sequencer for the sequence
         sequencer = MidiSystem.getSequencer();
         sequencer.open();
      }
      catch (MidiUnavailableException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   public final void addActionListener(ControllerEventListener listener)
   {
      int[] arg1 = { 127 };
      sequencer.addControllerEventListener(listener, arg1);
   }

   public final Sequencer getSequencer()
   {
      return sequencer;
   }

   public final boolean isRunning()
   {
      return sequencer.isRunning();
   }

   @Override
   public void run()
   {
      try
      {
         // From file
         sequence = MidiSystem.getSequence(new File(iFileName));

         // From URL
         // Sequence sequence = MidiSystem.getSequence(new
         // URL("http://hostname/midiaudiofile"));

         sequencer.setSequence(sequence);
         sequencer.setTempoInBPM(iTempo);

         // Start playing
         sequencer.start();
      }
      catch (MalformedURLException e)
      {
      }
      catch (IOException e)
      {
      }
      catch (InvalidMidiDataException e)
      {
      }

   }

   public void stopPlaying()
   {
      sequencer.stop();
      sequencer.close();
   }

}
