package core.midi;

import java.util.List;

import javax.sound.midi.Sequencer;
import javax.sound.sampled.Clip;
import javax.swing.JButton;

import ui.main.WhistleStudio;

public class WaitForSong extends Thread
{

   private Clip iClip = null;
   private List<JButton> iPlayList;
   private Sequencer iSequencer = null;
   private final List<JButton> iStopList;

   public WaitForSong(Clip clip, List<JButton> playList, List<JButton> stopList)
   {
      iClip = clip;
      iClip.setFramePosition(0);
      iPlayList = playList;
      iStopList = stopList;
   }

   public WaitForSong(Sequencer sequencer, List<JButton> playList,
         List<JButton> stopList)
   {
      iSequencer = sequencer;
      iPlayList = playList;
      iStopList = stopList;
   }

   private void enableButtons()
   {
      WhistleStudio.enableButtons(iPlayList, true);
      WhistleStudio.enableButtons(iStopList, false);
   }

   @Override
   public void run()
   {
      waitASecond(1000);

      if (iSequencer != null)
      {
         while (iSequencer.isRunning())
         {
            waitASecond(100);
         }
         enableButtons();
      }

      if (iClip != null)
      {
         while (iClip.getFrameLength() > 0
               && iClip.getFramePosition() < iClip.getFrameLength())
         {
            waitASecond(100);
         }
         iClip.close();
         enableButtons();
      }
   }

   private static void waitASecond(int millis)
   {
      try
      {
         Thread.sleep(millis);
      }
      catch (InterruptedException e1)
      {
         e1.printStackTrace();
      }
   }

}
