package core.tones;

public final class FastFourierTransform
{

   private FastFourierTransform()
   {
   }

   private static int bitrev(int j, int nu)
   {
      int j2;
      int j1 = j;
      int k = 0;
      for (int i = 1; i <= nu; i++)
      {
         j2 = j1 / 2;
         k = 2 * k + j1 - 2 * j2;
         j1 = j2;
      }
      return k;
   }

   public static float[] doFFT(float[] sampleBuffer)
   {
      float[] fft = new float[sampleBuffer.length];
      fft = FastFourierTransform.fftMag(sampleBuffer);
      return fft;
   }

   public static float[] doFFT(int[] sampleBuffer)
   {
      float[] fft = new float[sampleBuffer.length];
      float[] fSamples = getFloatArray(sampleBuffer);
      fft = FastFourierTransform.fftMag(fSamples);
      return fft;
   }

   public static float[] fftMag(float[] x)
   {
      // assume n is a power of 2
      int n = x.length;
      int nu = (int) (Math.log(n) / Math.log(2));
      int n2 = n / 2;
      int nu1 = nu - 1;
      float[] xre = new float[n + 1];
      float[] xim = new float[n + 1];
      float[] mag = new float[n2 + 1];
      float tr, ti, p, arg, c, s;
      for (int i = 0; i < n; i++)
      {
         xre[i] = x[i];
         xim[i] = 0.0f;
      }
      int k = 0;

      for (int l = 1; l <= nu; l++)
      {
         while (k < n)
         {
            for (int i = 1; i <= n2; i++)
            {
               p = bitrev(k >> nu1, nu);
               arg = 2 * (float) Math.PI * p / n;
               c = (float) Math.cos(arg);
               s = (float) Math.sin(arg);
               tr = xre[k + n2] * c + xim[k + n2] * s;
               ti = xim[k + n2] * c - xre[k + n2] * s;
               xre[k + n2] = xre[k] - tr;
               xim[k + n2] = xim[k] - ti;
               xre[k] += tr;
               xim[k] += ti;
               k++;
            }
            k += n2;
         }
         k = 0;
         nu1--;
         n2 = n2 / 2;
      }
      k = 0;
      int r;
      while (k < n)
      {
         r = bitrev(k, nu);
         if (r > k)
         {
            tr = xre[k];
            ti = xim[k];
            xre[k] = xre[r];
            xim[k] = xim[r];
            xre[r] = tr;
            xim[r] = ti;
         }
         k++;
      }
      mag[0] = (float) (Math.sqrt(xre[0] * xre[0] + xim[0] * xim[0])) / n;
      for (int i = 1; i < n / 2; i++)
      {
         mag[i] =
               2 * (float) (Math.sqrt(xre[i] * xre[i] + xim[i] * xim[i])) / n;
      }
      return mag;
   }

   private static float[] getFloatArray(int[] sampleBuffer)
   {
      float[] fSamples = new float[sampleBuffer.length];
      for (int i = 0, n = sampleBuffer.length; i < n; i++)
      {
         fSamples[i] = sampleBuffer[i];
      }
      return fSamples;
   }

   public static long getHertz(float[] sampleBuffer, float sampleRate, int minHertz, int maxHertz)
   {
      float[] fft = doFFT(sampleBuffer);

      double sample = 1 / (fft.length / sampleRate);

      int minH = (int) Math.round(minHertz / sample);
      int maxH = (int) Math.round(maxHertz / sample);

      float max = Float.MIN_VALUE;
      int index = 0;
      for (int i = minH; i <= maxH; i++)
      {
         if (fft[i] > max)
         {
            max = fft[i];
            index = i;
         }
      }

      double hertz = Math.round(index * sample);

      return Math.round(hertz);
   }

   public static long getHertz(int[] sampleBuffer, float sampleRate, int minHertz, int maxHertz)
   {
      float[] fSamples = getFloatArray(sampleBuffer);
      return getHertz(fSamples, sampleRate, minHertz, maxHertz);
   }
}
