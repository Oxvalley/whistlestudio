package core.tones;

import java.util.ArrayList;
import java.util.List;

import core.wav.SliceData;
import core.wav.WaveData;
import ui.main.WaveProperties;

public class FrequenceFinder
{

   private static final double C0_HERTZ = 16.351562;
   private static String[] toneNames =
         { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };

   public static double getCent(double newFreq)
   {
      return getCent(C0_HERTZ, newFreq);
   }

   public static double getCent(double knownFreq, double newFreq)
   {
      return Math.log(newFreq / knownFreq) / Math.log(2.0) * 1200;
   }

   // List<Note> noteArr = new ArrayList<Note>();

   public static Note getClosestNote(double hertz)
   {
      double cent = getCent(hertz);
      long index = getClosestToneIndex(cent);
      int octave = (int) (index / 12);
      int note = (int) index % 12;
      if (note < 0)
      {
         System.out.println("Bad Hertz: " + hertz + ", cent=" + cent
               + ", index=" + index + ", octave=" + octave);
         return null;
      }
      return new Note(toneNames[note] + octave, getHertz(index * 100));

   }

   public static long getClosestToneIndex(double cent)
   {
      return Math.round(cent / 100.0);

   }

   private static double getHertz(double startHertz, long cent)
   {
      double pow = cent / 1200.0;
      return startHertz * Math.pow(2.0, pow);
   }

   private static double getHertz(long cent)
   {
      return getHertz(C0_HERTZ, cent);
   }

   private final List<SliceData> iDerives;

   private List<ToneData> lst;

   public FrequenceFinder(List<SliceData> derives)
   {
      iDerives = derives;
      // noteArr.add(new Note("C0", 16.35));
      // noteArr.add(new Note("C#", 17.32));
      // noteArr.add(new Note("D0", 18.35));
      // noteArr.add(new Note("D#", 19.45));
      // noteArr.add(new Note("E0", 20.60));
      // noteArr.add(new Note("F0", 21.83));
      // noteArr.add(new Note("F#", 23.12));
      // noteArr.add(new Note("G0", 24.50));
      // noteArr.add(new Note("G#", 25.96));
      // noteArr.add(new Note("A0", 27.50));
      // noteArr.add(new Note("A#", 29.14));
      // noteArr.add(new Note("B0", 30.87));
      // noteArr.add(new Note("C1", 32.70));
      // noteArr.add(new Note("C#1", 34.65));
      // noteArr.add(new Note("D1", 36.71));
      // noteArr.add(new Note("D#1", 38.89));
      // noteArr.add(new Note("E1", 41.20));
      // noteArr.add(new Note("F1", 43.65));
      // noteArr.add(new Note("F#1", 46.25));
      // noteArr.add(new Note("G1", 49.00));
      // noteArr.add(new Note("G#1", 51.91));
      // noteArr.add(new Note("A1", 55.00));
      // noteArr.add(new Note("A#1", 58.27));
      // noteArr.add(new Note("B1", 61.74));
      // noteArr.add(new Note("C2", 65.41));
      // noteArr.add(new Note("C#2", 69.30));
      // noteArr.add(new Note("D2", 73.42));
      // noteArr.add(new Note("D#2", 77.78));
      // noteArr.add(new Note("E2", 82.41));
      // noteArr.add(new Note("F2", 87.31));
      // noteArr.add(new Note("F#2", 92.50));
      // noteArr.add(new Note("G2", 98.00));
      // noteArr.add(new Note("G#2", 103.83));
      // noteArr.add(new Note("A2", 110.00));
      // noteArr.add(new Note("A#2", 116.54));
      // noteArr.add(new Note("B2", 123.47));
      // noteArr.add(new Note("C3", 130.81));
      // noteArr.add(new Note("C#3", 138.59));
      // noteArr.add(new Note("D3", 146.83));
      // noteArr.add(new Note("D#3", 155.56));
      // noteArr.add(new Note("E3", 164.81));
      // noteArr.add(new Note("F3", 174.61));
      // noteArr.add(new Note("F#3", 185.00));
      // noteArr.add(new Note("G3", 196.00));
      // noteArr.add(new Note("G#3", 207.65));
      // noteArr.add(new Note("A3", 220.00));
      // noteArr.add(new Note("A#3", 233.08));
      // noteArr.add(new Note("B3", 246.94));
      // noteArr.add(new Note("C4", 261.63));
      // noteArr.add(new Note("C#4", 277.18));
      // noteArr.add(new Note("D4", 293.66));
      // noteArr.add(new Note("D#4", 311.13));
      // noteArr.add(new Note("E4", 329.63));
      // noteArr.add(new Note("F4", 349.23));
      // noteArr.add(new Note("F#4", 369.99));
      // noteArr.add(new Note("G4", 392.00));
      // noteArr.add(new Note("G#4", 415.30));
      // noteArr.add(new Note("A4", 440.00));
      // noteArr.add(new Note("A#4", 466.16));
      // noteArr.add(new Note("B4", 493.88));
      // noteArr.add(new Note("C5", 523.25));
      // noteArr.add(new Note("C#5", 554.37));
      // noteArr.add(new Note("D5", 587.33));
      // noteArr.add(new Note("D#5", 622.25));
      // noteArr.add(new Note("E5", 659.26));
      // noteArr.add(new Note("F5", 698.46));
      // noteArr.add(new Note("F#5", 739.99));
      // noteArr.add(new Note("G5", 783.99));
      // noteArr.add(new Note("G#5", 830.61));
      // noteArr.add(new Note("A5", 880.00));
      // noteArr.add(new Note("A#5", 932.33));
      // noteArr.add(new Note("B5", 987.77));
      // noteArr.add(new Note("C6", 1046.50));
      // noteArr.add(new Note("C#6", 1108.73));
      // noteArr.add(new Note("D6", 1174.66));
      // noteArr.add(new Note("D#6", 1244.51));
      // noteArr.add(new Note("E6", 1318.51));
      // noteArr.add(new Note("F6", 1396.91));
      // noteArr.add(new Note("F#6", 1479.98));
      // noteArr.add(new Note("G6", 1567.98));
      // noteArr.add(new Note("G#6", 1661.22));
      // noteArr.add(new Note("A6", 1760.00));
      // noteArr.add(new Note("A#6", 1864.66));
      // noteArr.add(new Note("B6", 1975.53));
      // noteArr.add(new Note("C7", 2093.00));
      // noteArr.add(new Note("C#7", 2217.46));
      // noteArr.add(new Note("D7", 2349.32));
      // noteArr.add(new Note("D#7", 2489.02));
      // noteArr.add(new Note("E7", 2637.02));
      // noteArr.add(new Note("F7", 2793.83));
      // noteArr.add(new Note("F#7", 2959.96));
      // noteArr.add(new Note("G7", 3135.96));
      // noteArr.add(new Note("G#7", 3322.44));
      // noteArr.add(new Note("A7", 3520.00));
      // noteArr.add(new Note("A#7", 3729.31));
      // noteArr.add(new Note("B7", 3951.07));
      // noteArr.add(new Note("C8", 4186.01));
      // noteArr.add(new Note("C#8", 4434.92));
      // noteArr.add(new Note("D8", 4698.64));
      // noteArr.add(new Note("D#8", 4978.03));
   }

   private static void addHertzInfo(List<ToneData> lst2, int minHertz,
         int maxHertz)
   {
      // int i = 0;
      ToneData oldToneData = null;
      for (ToneData toneData : lst2)
      {
         if (toneData.isTone())
         {
            long hertz = getHertz(toneData, 0.5f, minHertz, maxHertz);
            toneData.setHertz(hertz);

            Note note = getClosestNote(hertz);

            toneData.setNote(note);
            if (note == null)
            {
               toneData.setIsNoTone();
            }

            // i++;
         }
         else
         {
            // Silent
            if (oldToneData != null && oldToneData.isTone())
            {
               oldToneData.setSilentAfterwards(toneData);
            }
         }
         oldToneData = toneData;
      }
   }

   private static void calculateNoteLengths(List<ToneData> lst2)
   {
      for (ToneData toneData : lst2)
      {
         toneData.calcNoteLength();
      }
   }

   public List<ToneData> createNoteList(int minHertz, int maxHertz)
   {
      lst = new ArrayList<ToneData>();
      boolean inTone = false;
      ToneData toneData = null;
      for (SliceData slice : iDerives)
      {
         if (inTone)
         {
            inTone = slice.isTone();
            // In a tone
            if (slice.isTone())
            {
               // Tone continuous
               toneData.add(slice);
            }
            else
            {
               // Tone just ended
               toneData = saveAndCreateNewToneData(toneData, lst, slice);
            }
         }
         else
         {
            inTone = slice.isTone();
            // Not in a tone
            if (slice.isTone())
            {
               // Tone start
               toneData = saveAndCreateNewToneData(toneData, lst, slice);
            }
            else
            {
               // Not a tone is continuing
               if (toneData == null)
               {
                  toneData = saveAndCreateNewToneData(toneData, lst, slice);
               }
               else
               {
                  toneData.add(slice);
               }
            }
         }
      }
      // Save last note
      if (toneData != null)
      {
         lst.add(toneData);
      }

      calculateNoteLengths(lst);

      addHertzInfo(lst, minHertz, maxHertz);

      printInfo(lst);

      return lst;

   }

   private static long getHertz(ToneData toneData, float cut, int minHertz,
         int maxHertz)
   {
      toneData.setArray(WaveData.getDataArray(toneData, true, cut));
      long hertz = FastFourierTransform.getHertz(toneData.getArray(),
            toneData.getSampleRate(), minHertz, maxHertz);
      return hertz;
   }

   private static void printInfo(List<ToneData> lst2)
   {
      double realDurations = 0.0;
      double realSilenceDurations = 0.0;
      double calcDurations = 0.0;
      double calcSilenceDurations = 0.0;

      for (ToneData toneData : lst2)
      {
         if (toneData != null)
         {
            System.out.println(toneData.toString());
            realDurations += toneData.getDurationInMilliSeconds();
            calcDurations += toneData.getNoteLength().getMultiple();
                       if (toneData.getSilentAfterwards() != null)
            {
               realSilenceDurations +=
                     toneData.getSilentAfterwards().getDurationInMilliSeconds();
               calcSilenceDurations += toneData.getSilentAfterwards()
                     .getNoteLength().getMultiple();
            }
         }
      }

      System.out
            .println("\nTotal time real tones (ms)      : " + realDurations);
      System.out.println("Total time best fit tones (ms)  : " + calcDurations);
      System.out.println(
            "Total time real silence (ms)    : " + realSilenceDurations);
      System.out.println(
            "Total time best fit silence (ms): " + calcSilenceDurations);
   }

   private static ToneData saveAndCreateNewToneData(ToneData toneData,
         List<ToneData> lst2, SliceData slice)
   {
      ToneData toneData2 = toneData;
      if (toneData != null)
      {
         // Save old ToneData
         lst2.add(toneData);
      }
      toneData2 = new ToneData(slice, WaveData.getSampleRate(), WaveData.getSamplebits());
      return toneData2;
   }

   public static String getToneName(int index)
   {
      return toneNames[index];
   }

   public static String[] getToneNames()
   {
      return toneNames;
   }

   public List<ToneData> createNoteList()
   {
      WaveProperties wp = WaveProperties.getInstance();
      int minHertz = wp.getMinHertz();
      int maxHertz = wp.getMaxHertz();

      return createNoteList(minHertz, maxHertz);
   }

}
