package core.tones;

import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.io.File;
import java.util.HashMap;

import javax.swing.JComponent;

public class ImagePalette extends HashMap<PaletteEnum, Image>
{

   private Toolkit iToolkit;
   private JComponent iComponent;

   public ImagePalette(JComponent component)
   {
      iComponent = component;
      iToolkit = Toolkit.getDefaultToolkit();

      put(PaletteEnum.higher, "higher3.jpg");
      put(PaletteEnum.lower, "lower3.jpg");
   }

   private void put(PaletteEnum name, String filename)
   {
      put(name, getImage(filename));
   }

   public Image getImage(String fileName)
   {
      String filePath = "images/" + fileName;
      File file = new File(filePath);
      if (file.exists())
      {
         Image track = iToolkit.getImage(filePath);
         MediaTracker mediaTracker = new MediaTracker(iComponent);
         mediaTracker.addImage(track, 1);

         try
         {
            mediaTracker.waitForID(1);
         }
         catch (InterruptedException ie)
         {
            System.out.println("Could not load image " + filePath);
            return null;
         }
         if (track.getWidth(null) == -1)
         {
            System.out.println("Could not load image " + filePath
                  + " . Width=-1");
            return null;

         }
         return track;
      }
      else
      {
         System.out.println("File " + file.getAbsolutePath()
               + " does not exist!");
         return null;
      }
   }

}
