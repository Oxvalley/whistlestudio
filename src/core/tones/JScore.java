package core.tones;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

public class JScore extends JTone
{
   private ImagePalette iImagepalette = new ImagePalette(this);

   public JScore(ToneData toneData)
   {
      super(toneData);
   }

   @Override
   public void paint(Graphics g)
   {
      int topX = SELECTED_FRAME_SIZE;
      int topY = SELECTED_FRAME_SIZE;
      int totalWidth = getWidth() - (2 * SELECTED_FRAME_SIZE) - 1;
      int totalHeight = getHeight() - (2 * SELECTED_FRAME_SIZE) - 1;

      Graphics2D g2 = (Graphics2D) g;
      if (iSelected)
      {
         // Blue filled rectangle across if selected
         g2.setColor(Color.BLUE);
         g2.fillRect(topX - 5, topY - 5, totalWidth + 10, totalHeight + 10);
      }

      // Red Rectangle to mark outer area
      g2.setColor(Color.RED);
      g2.drawRect(topX, topY, totalWidth, totalHeight);

      // Sign
      int signXStart = 0;
      int signYStart = 0;
      int signHeight = 0;
      int signWidth = 0;
      Image signImage = null;

      switch (getToneAttributes().getSigns())
      {
         case Nothing:

            break;

         case Higher:
            signXStart = (int) Math.round(topX + (totalWidth * 0.05));
            signYStart = (int) Math.round(topY + (totalHeight * 0.65));
            signWidth = (int) Math.round(totalWidth * 0.4);
            signHeight = (int) Math.round(topY + (totalHeight * 0.3));
            signImage = iImagepalette.get(PaletteEnum.higher);

            break;

         case Lower:
            signXStart = (int) Math.round(topX + (totalWidth * 0.15));
            signYStart = (int) Math.round(topY + (totalHeight * 0.65));
            signWidth = (int) Math.round(totalWidth * 0.3);
            signHeight = (int) Math.round(topY + (totalHeight * 0.3));
            signImage = iImagepalette.get(PaletteEnum.lower);

            break;
      }

      if (isDownNote())
      {
         // DownNote

         // Note oval
         g2.setColor(Color.BLACK);

         int ovalXStart = (int) Math.round(topX + (totalWidth * 0.5));
         int ovalYStart = (int) Math.round(topY + (totalHeight * 0.7));
         int ovalWidth = (int) Math.round(totalWidth * 0.5);
         int ovalHeight = (int) Math.round(topY + (totalHeight * 0.3));

         g2.fillOval(ovalXStart, ovalYStart, ovalWidth, ovalHeight);

         // Handle
         int handleXStart = (int) Math.round(topX + (totalWidth * 0.95));
         int handleYStart = (int) Math.round(topY + (totalHeight * 0.05));
         int handleWidth = (int) Math.round(totalWidth * 0.05);
         int handleHeight =
               (int) Math.round(ovalYStart + (ovalHeight * 0.5) - handleYStart);
         g2.fillRect(handleXStart, handleYStart, handleWidth, handleHeight);
      }
      else
      {
         // UpNote

         // Note oval
         g2.setColor(Color.BLACK);

         int ovalXStart = (int) Math.round(topX + (totalWidth * 0.5));
         int ovalYStart = (int) Math.round(topY * 1.0);
         int ovalWidth = (int) Math.round(totalWidth * 0.5);
         int ovalHeight = (int) Math.round(topY + totalHeight * 0.3);

         g2.fillOval(ovalXStart, ovalYStart, ovalWidth, ovalHeight);

         // Handle
         int handleXStart = (int) Math.round(ovalXStart * 1.0);
         int handleYStart =
               (int) Math.round(topY + (ovalYStart + ovalHeight * 0.5));
         int handleWidth = (int) Math.round(totalWidth * 0.05);
         int handleHeight = (int) Math.round((totalHeight * 0.8));
         g2.fillRect(handleXStart, handleYStart, handleWidth, handleHeight);

         // recalculate height of sign
         signYStart -= (int) Math.round((totalHeight * 0.6));

      }

      // Draw sign
      g2.drawImage(signImage, signXStart, signYStart, signWidth, signHeight,
            null);

   }

}
