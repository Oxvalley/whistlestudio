package core.tones;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;

import common.ui.components.ext.JScrollPaneExt;

public class JScorePanel extends JTonePanel
{

   private double iZoom;

   public JScorePanel(int width, int height, double zoom)
   {
      this(width, height, null, zoom);
   }

   public JScorePanel(int width, int height, JScrollPaneExt scroll, double zoom)
   {
      super(width, height);
      iZoom = zoom;
      iScroll = scroll;
      if (scroll == null)
      {
         iResizeable = true;
      }
      else
      {
         iResizeable = false;
      }
      setBackground(Color.WHITE);
      iSize = new Dimension(7500, 750);
      iToneType = ToneTypeEnum.note;
   }

   @Override
   public void paint(Graphics g)
   {
      super.paint(g);
      resetBounds();
   }

   private void resetBounds()
   {
      boolean firstNoteFound;
      int w;
      int h;
      double xBlock;
      double yBlock;
      int width = Zoom.getSizeInt(iZoom, 50);

      if (iResizeable)
      {
         w = getWidth();
         h = getHeight();
         xBlock = ((double) (w - 2 * JTone.FRAME_SIZE) / iToneLengthSum);
         yBlock =
               ((double) (h - 2 * JTone.FRAME_SIZE) / (iMax_index - iMin_index + 1));
      }
      else
      {
         Dimension dim = getRequiredSize();
         w = dim.width;
         h = dim.height;
         xBlock = Zoom.getSize(iZoom, 0.4);
         width = (int) Math.round(xBlock * 87.5);
         yBlock = Zoom.getSize(iZoom, 40);
      }

      long accuToneLength = 0;
      firstNoteFound = false;
      if (iJToneList != null)
      {
         Rectangle rect = new Rectangle();
         for (JToneInterface jtone : iJToneList)
         {
            if (jtone.isTone())
            {
               firstNoteFound = true;
               int x =
                     (int) Math.round(accuToneLength * xBlock)
                           + JTone.FRAME_SIZE;
               int y =
                     JTone.FRAME_SIZE
                           + (int) Math.round(yBlock
                                 * (iMax_index - jtone.getMidiToneIndex()));
               int height = (int) Math.round(yBlock);

               String name = jtone.getName();
               if (name != null)
               {
                  ToneAttributes toneAtt = jtone.getToneAttributes();

                  y = toneAtt.getYPos();

                  int downNoteYPlus = Zoom.getSizeInt(iZoom, 30);
                  if (jtone.isDownNote())
                  {
                     downNoteYPlus = 0;
                  }
                  jtone.setBounds(x - JTone.SELECTED_FRAME_SIZE, (int) Math.round((55 - y) * Zoom.getSize(iZoom, 6.0)
                        - JTone.SELECTED_FRAME_SIZE + downNoteYPlus),
                        (int) Math.round(width * xBlock)
                              + JTone.SELECTED_FRAME_SIZE + 1, height
                              + JTone.SELECTED_FRAME_SIZE + 1);
                  rect.add(jtone.getBounds());
                  if (firstNoteFound)
                  {
                     accuToneLength += (width * 2.5);
                  }
               }
            }
         }
         iSize = new Dimension(rect.width, rect.height);

      }
   }

   public static void main(String[] args)
   {
      printYFor("C1");
      printYFor("C#1");
      printYFor("D1");
      printYFor("E5");
      printYFor("F5");
      printYFor("G5");
      printYFor("A5");
      printYFor("A#5");
      printYFor("B5");
      printYFor("C6");
      printYFor("C7");
   }

   private static void printYFor(String toneName)
   {
      System.out.println(toneName + "=" + Note.getToneYOnScale(toneName));

   }
}
