package core.tones;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;

public class JTone extends JComponent implements MouseListener, JToneInterface
{

   /**
    *
    */
   private static final long serialVersionUID = 3615272007027691445L;
   public static final int FRAME_SIZE = 10;
   protected static final int ROUNDED_SIZE = 10;
   public static final int SELECTED_FRAME_SIZE = 2;
   protected boolean iSelected = false;
   protected final ToneData iToneData;
   private int iOrigMidiToneIndex = -1;

   public JTone(ToneData toneData)
   {
      iToneData = toneData;
      setToolTipText(getName() + " " + getLength() + "-> " + getMidiToneIndex());
      addMouseListener(this);
   }

   private String getLength()
   {
      if (iToneData != null)
      {
         if (iToneData.getNoteLength() != null)
         {
            return iToneData.getNoteLength().getName();
         }
      }
      return "";
   }

   @Override
   public int getMidiToneIndex()
   {
      return iToneData.getNote().getMidiToneIndex();
   }

   @Override
   public double getMillLength()
   {
      return iToneData.getNoteLength().getMultiple();
   }

   @Override
   public String getName()
   {
      return iToneData.getNote().getName();
   }

   @Override
   public int getPlayedToneIndex()
   {
      return iToneData.getPlayedToneIndex();
   }

   @Override
   public boolean isTone()
   {
      return iToneData.isTone();
   }

   @Override
   public void mouseClicked(MouseEvent e)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void mouseEntered(MouseEvent e)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void mouseExited(MouseEvent e)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void mousePressed(MouseEvent e)
   {
      ((JTonePanel) getParent()).setSelected(this);
      if (e.getClickCount() == 2)
      {
         JToneInfoFrame frm = new JToneInfoFrame(this);
         frm.setVisible(true);
      }

      getParent().repaint();
   }

   @Override
   public void mouseReleased(MouseEvent e)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void paint(Graphics g)
   {
      super.paint(g);
      int x = SELECTED_FRAME_SIZE;
      int y = SELECTED_FRAME_SIZE;
      int width = getWidth() - (2 * SELECTED_FRAME_SIZE) - 1;
      int height = getHeight() - (2 * SELECTED_FRAME_SIZE) - 1;

      Graphics2D g2 = (Graphics2D) g;
      if (iSelected)
      {
         g2.setColor(Color.BLUE);
         g2.fillRoundRect(x - 5, y - 5, width + 10, height + 10,
               ROUNDED_SIZE * 3, ROUNDED_SIZE * 3);
      }
      g2.setColor(Color.RED);
      g2.fillRoundRect(x, y, width, height, ROUNDED_SIZE, ROUNDED_SIZE);
      g2.setColor(Color.LIGHT_GRAY);
      g2.drawRoundRect(x, y, width, height, ROUNDED_SIZE, ROUNDED_SIZE);
      g2.setColor(Color.BLACK);
      g2.drawString(getName(), SELECTED_FRAME_SIZE + FRAME_SIZE,
            SELECTED_FRAME_SIZE + (getHeight() / 2) - 8 + FRAME_SIZE);

   }

   @Override
   public void setMidiToneIndex(int value)
   {
      iToneData.getNote().setMidiToneIndex(value);

   }

   @Override
   public void setSelected(boolean value)
   {
      iSelected = value;
   }

   public ToneData getToneData()
   {
      return iToneData;
   }

   @Override
   public boolean isDownNote()
   {
      return Note.isDownNote(getMidiToneIndex());
   }

   @Override
   public ToneAttributes getToneAttributes()
   {
      return Note.getToneYOnScale(getName());
   }

   @Override
   public int getOrigMidiToneIndex()
   {
      return iOrigMidiToneIndex;
   }

   @Override
   public void setOrigMidiToneIndex(int index)
   {
      iOrigMidiToneIndex = index;
   }

}
