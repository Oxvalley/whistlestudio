package core.tones;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;

import common.ui.components.ext.CompExtEnum;
import common.ui.components.ext.JPanelExt;
import common.utils.GraphicsUtil;
import ui.main.WhistleStudio;

public class JToneInfoFrame extends JFrame implements MouseMotionListener
{

   /**
    *
    */
   private static final long serialVersionUID = 1188313304519730918L;
   private JTone ijTone;
   private JEditorPane txtInfo;

   public JToneInfoFrame(JTone jTone)
   {
      this();
      ijTone = jTone;
      setInfo();
   }

   private void setInfo()
   {
      ToneData tone = ijTone.getToneData();
      Note note = tone.getNote();
      NoteLength length = tone.getNoteLength();
      ToneData silence = tone.getSilentAfterwards();

      String html = "<html>\n";
      html += "<body bgcolor=\"#00FFFF\">\n";

      html +=
            "<p ALIGN=\"LEFT\"><b>Note Number</b>: "
                  + ijTone.getPlayedToneIndex() + "<br>\n";
      html +=
            "<b>Best Fit Tone Name: </b><span style=\"background-color: #FFFF00\">"
                  + note.getName() + "</span><br>\n";
      html += "<b>Best Fit Freq (hz):</b>" + note.getHertz() + "<br>\n";
      html +=
            "<b>Best Fit Note Length (ms): </b>" + length.getMultiple()
                  + "<br>\n";
      html +=
            "<b>Best Fit Note Length Name: </b> <span style=\"background-color: #FFFF00\">"
                  + length.getName() + "</span></p>\n";
      html +=
            "<p><b>Midi Key Index:</b> " + ijTone.getMidiToneIndex() + "<br>\n";
      html += "<b>Midi Velocity: </b>" + tone.getVelocity() + "<br>\n";
      html += "<b>Midi StartTick: </b>" + tone.getStartTick() + "<br>\n";
      html += "<b>Midi TickLength: </b>" + tone.getTickLength() + "</p>\n";
      html += "<p><b>Original Freq (hz): </b>" + tone.getHertz() + "<br>\n";
      html +=
            "<b>Original Tone Start (ms): </b>" + tone.getStartIndex()
                  * tone.getTimeFactor() + "<br>\n";
      html +=
            "<b>Original Duration(ms): </b>" + tone.getDurationInMilliSeconds()
                  + "</p>\n";
      html +=
            "<p><b>Silence Afterwards Start (ms): </b>" + tone.getStartIndex()
                  * silence.getTimeFactor() + "<br>\n";
      html +=
            "<b>Silence Afterwards Duration(ms): </b>" + silence.getDurationInMilliSeconds()
                  + "<br>\n";
      html +=
            "<b>Silence Afterwards Best Fit Note Length (ms): </b>"
                  + silence.getNoteLength().getMultiple() + "<br>\n";
      html += "</body>";
      html += "</html>";
      txtInfo.setText(html);
   }

   public JToneInfoFrame()
   {
      super("Tone Information");
      setIconImage(getToolkit().getImage(WhistleStudio.ICON_NAME));
      addMouseMotionListener(this);
      int width = 500;
      int height = 500;
      setSize(width, height);
      JPanelExt panel = new JPanelExt(width, height);

      getContentPane().add(panel);

      JButton btnPrevious = new JButton("Prev");
      GraphicsUtil.adjustFontForOS(btnPrevious);
      panel.addComponentExt(btnPrevious, 10, 150, 60,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      txtInfo = new JEditorPane();
      txtInfo.setContentType("text/html");
      txtInfo.setEditable(false);
      panel.addComponentExt(txtInfo, 80, 10, panel.getCompExtWidth() - 160,
            panel.getCompExtHeight() - 20, CompExtEnum.ResizeWithWidthAndHeight);

      JButton btnNext = new JButton("Next");
      GraphicsUtil.adjustFontForOS(btnNext);
      panel.addComponentExt(btnNext, panel.getCompExtWidth() - 70, 150, 60,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.MoveWithWidth);

   }

   @Override
   public void mouseDragged(MouseEvent arg0)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void mouseMoved(MouseEvent arg0)
   {
      // TODO Auto-generated method stub

   }

}
