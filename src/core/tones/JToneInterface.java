package core.tones;

import java.awt.Rectangle;

public interface JToneInterface
{

   int getMidiToneIndex();

   void setMidiToneIndex(int i);

   int getPlayedToneIndex();

   double getMillLength();

   boolean isTone();

   Rectangle getBounds();

   void setBounds(int i, int j, int k, int l);

   void setSelected(boolean b);

   String getName();

   boolean isDownNote();

   ToneAttributes getToneAttributes();

   int getOrigMidiToneIndex();

   void setOrigMidiToneIndex(int index);
}
