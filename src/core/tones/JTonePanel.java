package core.tones;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.ControllerEventListener;
import javax.sound.midi.ShortMessage;

import common.ui.components.ext.JPanelExt;
import common.ui.components.ext.JScrollPaneExt;
import ui.main.WaveProperties;

public class JTonePanel extends JPanelExt implements ControllerEventListener
{

   /**
    *
    */
   private static final long serialVersionUID = -1191831709152689864L;
   protected ArrayList<JToneInterface> iJToneList;
   protected int iMax_index;
   protected int iMin_index;
   protected boolean iResizeable = false;
   protected JScrollPaneExt iScroll;
   protected Dimension iSize;
   protected int iToneLengthSum;
   private int toneCounter = 0;
   protected ToneTypeEnum iToneType;
   private ArrayList<JToneInterface> origToneList;
   private int iOrig_Max_index;
   private int iOrig_Min_index;

   public JTonePanel(int width, int height)
   {
      this(width, height, null);
   }

   public JTonePanel(int width, int height, JScrollPaneExt scroll)
   {
      super(width, height);
      iScroll = scroll;
      if (scroll == null)
      {
         iResizeable = true;
      }
      else
      {
         iResizeable = false;
      }
      setBackground(Color.YELLOW);
      iSize = new Dimension(7500, 750);
      iToneType = ToneTypeEnum.square;
   }


   public void saveIndexAsOrigIndex()
   {
      for (JToneInterface jtone : iJToneList)
      {
            jtone.setOrigMidiToneIndex(jtone.getMidiToneIndex());
      }
      iOrig_Max_index = iMax_index;
      iOrig_Min_index = iMin_index;
   }

   public void moveToneIndex(int octave, int halftone)
   {
      moveToneIndex(octave * 12 + halftone);
   }

   public void moveToneIndex(int diffindex)
   {
      if (iJToneList != null)
      {
         for (JToneInterface jtone : iJToneList)
         {
            jtone.setMidiToneIndex(jtone.getOrigMidiToneIndex() + diffindex);
         }
         iMax_index = iOrig_Max_index + diffindex;
         iMin_index = iOrig_Min_index + diffindex;
         resetBounds();
      }
   }

   public JToneInterface getNextJTone(JTone previous)
   {

      int index = getJToneIndex(previous);
      if (index == -1)
      {
         return null;
      }

      if (index >= iJToneList.size() - 1)
      {
         return null;
      }

      return iJToneList.get(index + 1);
   }

   private int getJToneIndex(JTone previous)
   {

      for (int index = 0; index <= iJToneList.size(); index++)
      {
         JToneInterface jtone = iJToneList.get(index);
         if (jtone.equals(previous))
         {
            return index;
         }
      }
      return -1;
   }

   @Override
   public void controlChange(ShortMessage arg0)
   {

      if (arg0.getData2() == 1)
      {
         // Note is starting
         JToneInterface jtone = getPlayedTone(toneCounter);
         setSelected(jtone);
      }
      else
      {
         // Note is ending
         setSelected(null);
         toneCounter++;
      }
      repaint();
   }

   @Override
   public Dimension getMaximumSize()
   {
      return iSize;
   }

   @Override
   public Dimension getMinimumSize()
   {
      return getMaximumSize();
   }

   private JToneInterface getPlayedTone(int index)
   {
      for (JToneInterface jtone : iJToneList)
      {
         if (jtone.getPlayedToneIndex() == index)
         {
            return jtone;
         }
      }
      return null;
   }

   @Override
   public Dimension getPreferredSize()
   {
      return getMaximumSize();
   }

   protected Dimension getRequiredSize()
   {
      return iSize;
   }

   public void init(List<ToneData> toneDataList, boolean firstPanel)
   {
      if (toneDataList == null || toneDataList.size() == 0)
      {
         return;
      }

      removeAll();
      iJToneList = new ArrayList<JToneInterface>();
      iToneLengthSum = 0;
      iMax_index = -1;
      iMin_index = Integer.MAX_VALUE;
      boolean firstNoteFound = false;

      for (ToneData toneData : toneDataList)
      {
         JToneInterface jtone = null;
         switch (iToneType)
         {
            case square:
               jtone = new JTone(toneData);
               break;

            case note:
               jtone = new JScore(toneData);
               break;
         }

         add((Component) jtone);
         iJToneList.add(jtone);

         if (toneData.isTone() && toneData.getNote() != null)
         {

            firstNoteFound = true;

            if (iMax_index < toneData.getNote().getMidiToneIndex())
            {
               iMax_index = toneData.getNote().getMidiToneIndex();
            }

            if (iMin_index > toneData.getNote().getMidiToneIndex())
            {
               iMin_index = toneData.getNote().getMidiToneIndex();
            }
         }
         if (firstNoteFound)
         {
            iToneLengthSum += toneData.getNoteLength().getMultiple();
         }
      }

      if (firstPanel)
      {
         saveIndexAsOrigIndex();
         WaveProperties wp = WaveProperties.getInstance();
         moveToneIndex(wp.getOctave(), wp.getTone());
         resetBounds();
         getParent().repaint();
      }
   }

   @Override
   public void paint(Graphics g)
   {
      super.paint(g);
      resetBounds();
   }

   private void resetBounds()
   {
      boolean firstNoteFound;
      int w;
      int h;
      double xBlock;
      double yBlock;
      if (iResizeable)
      {
         w = getWidth();
         h = getHeight();
         xBlock = ((double) (w - 2 * JTone.FRAME_SIZE) / iToneLengthSum);
         yBlock = ((double) (h - 2 * JTone.FRAME_SIZE)
               / (iMax_index - iMin_index + 1));
      }
      else
      {
         Dimension dim = getRequiredSize();
         w = dim.width;
         h = dim.height;
         xBlock = 0.45;
         yBlock = 28;
      }

      long accuToneLength = 0;
      firstNoteFound = false;
      if (iJToneList != null)
      {
         Rectangle rect = new Rectangle();
         for (JToneInterface jtone : iJToneList)
         {

            int width = (int) jtone.getMillLength();
            if (jtone.isTone())
            {
               firstNoteFound = true;
               int x = (int) Math.round(accuToneLength * xBlock)
                     + JTone.FRAME_SIZE;
               int y = JTone.FRAME_SIZE + (int) Math
                     .round(yBlock * (iMax_index - jtone.getMidiToneIndex()));
               int height = (int) Math.round(yBlock);
               jtone.setBounds(x - JTone.SELECTED_FRAME_SIZE,
                     y - JTone.SELECTED_FRAME_SIZE,
                     (int) Math.round(width * xBlock)
                           + JTone.SELECTED_FRAME_SIZE + 1,
                     height + JTone.SELECTED_FRAME_SIZE + 1);
               rect.add(jtone.getBounds());
            }
            if (firstNoteFound)
            {
               accuToneLength += width;
            }

         }
         iSize = new Dimension(rect.width, rect.height);

      }
   }

   public void resetScrollBars()
   {
      if (iScroll != null)
      {
         iScroll.getHorizontalScrollBar().setValue(0);
         iScroll.getVerticalScrollBar().setValue(0);
      }
      toneCounter = 0;
   }

   private void scrolltoView(JToneInterface jtone)
   {
      if (!iResizeable)
      {
         iScroll.scrollRectToVisible(jtone.getBounds());
      }

   }

   public void setSelected(JToneInterface selected)
   {
      for (JToneInterface jtone : iJToneList)
      {
         if (jtone == selected)
         {
            jtone.setSelected(true);
            scrolltoView(jtone);
         }
         else
         {
            jtone.setSelected(false);
         }
      }
   }

}
