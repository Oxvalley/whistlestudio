package core.tones;

import core.midi.MidiCreator.MidiTone;

public class Note
{
   private final double iHertz;
   private int iMidiToneIndex = -1;
   private String iName;

   public Note(String name, double hertz)
   {
      iName = name;
      iHertz = hertz;
      iMidiToneIndex = MidiTone.transformNoteNameToMidiToneIndex(iName);
   }

   public static boolean isDownNote(int midiToneIndex)
   {
      if (midiToneIndex <= 49 || (midiToneIndex >= 60 && midiToneIndex <= 69))
      {
         return true;
      }
      else
      {
         return false;
      }
   }

   public double getHertz()
   {
      return iHertz;
   }

   public int getMidiToneIndex()
   {
      return iMidiToneIndex;
   }

   public String getName()
   {
      return iName;
   }

   public void setMidiToneIndex(int midiToneIndex)
   {
      iMidiToneIndex = midiToneIndex;
      iName = MidiTone.transformMidiToneIndexToName(iMidiToneIndex);
   }

   public static ToneAttributes getToneYOnScale(String name)
   {
      int y;
      String tone = name.substring(0, name.length() - 1);
      String octave = name.substring(name.length() - 1, name.length());
      y = (Integer.valueOf(octave) * 7);
      ToneAttributes toneAtt = new ToneAttributes();

      if (tone.equals("C"))
      {
         toneAtt.setYPos(y + 0);
         toneAtt.setSigns(SignEnum.Nothing);
      }

      if (tone.equals("C#"))
      {
         toneAtt.setYPos(y + 0);
         toneAtt.setSigns(SignEnum.Higher);
      }

      if (tone.equals("D"))
      {
         toneAtt.setYPos(y + 1);
         toneAtt.setSigns(SignEnum.Nothing);
      }

      if (tone.equals("D#"))
      {
         toneAtt.setYPos(y + 2);
         toneAtt.setSigns(SignEnum.Lower);
      }

      if (tone.equals("E"))
      {
         toneAtt.setYPos(y + 2);
         toneAtt.setSigns(SignEnum.Nothing);
      }

      if (tone.equals("F"))
      {
         toneAtt.setYPos(y + 3);
         toneAtt.setSigns(SignEnum.Nothing);
      }

      if (tone.equals("F#"))
      {
         toneAtt.setYPos(y + 3);
         toneAtt.setSigns(SignEnum.Higher);
      }

      if (tone.equals("G"))
      {
         toneAtt.setYPos(y + 4);
         toneAtt.setSigns(SignEnum.Nothing);
      }

      if (tone.equals("G#"))
      {
         toneAtt.setYPos(y + 4);
         toneAtt.setSigns(SignEnum.Higher);
      }

      if (tone.equals("A"))
      {
         toneAtt.setYPos(y + 5);
         toneAtt.setSigns(SignEnum.Nothing);
      }

      if (tone.equals("A#"))
      {
         toneAtt.setYPos(y + 6);
         toneAtt.setSigns(SignEnum.Lower);
      }

      if (tone.equals("B"))
      {
         toneAtt.setYPos(y + 6);
         toneAtt.setSigns(SignEnum.Nothing);
      }

      return toneAtt;
   }

   @Override
   public final String toString()
   {
      return iName + " " + iHertz;
   }

}
