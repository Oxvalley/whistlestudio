package core.tones;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class NoteLength implements Comparator<NoteLength>
{
   private static NoteLength getNoteLength(double equalOrAboveValue)
   {
      NoteLength thisNoteLength = new NoteLength(1, 1, 1);
      if (equalOrAboveValue >= 1)
      {
         while (thisNoteLength.getQuote() < equalOrAboveValue)
         {
            thisNoteLength.multiply(2);
         }
      }
      else
      {
         while (thisNoteLength.getQuote() >= equalOrAboveValue)
         {
            thisNoteLength.halfMultiple();
         }
      }

      return thisNoteLength;
   }

   public static List<NoteLength> getNoteLengthList(double startValue,
         double endValue)
   {
      List<NoteLength> lst = new ArrayList<NoteLength>();

      NoteLength startNote = getNoteLength(startValue);
      NoteLength endNote = getNoteLength(endValue);
      NoteLength newNoteLength = startNote;
      lst.add(startNote);

      if (startValue >= endValue)
      {
         while (newNoteLength.getQuote() > endNote.getQuote())
         {
            newNoteLength = new NoteLength(newNoteLength);
            lst.add(newNoteLength);
            newNoteLength.halfMultiple();
         }
      }
      else
      {
         while (newNoteLength.getQuote() < endNote.getQuote())
         {
            newNoteLength = new NoteLength(newNoteLength);
            lst.add(newNoteLength);
            newNoteLength.multiply(2);
         }
      }

      return lst;
   }

   public static double getQuote(String toneSize)
   {
      if (!toneSize.contains("/"))
      {
         return -1;
      }

      int i = toneSize.indexOf("/");
      String top = toneSize.substring(0, i);
      String bottom = toneSize.substring(i + 1);
      return Double.parseDouble(top) / Double.parseDouble(bottom);
   }

   private double iFirstNumber;

   private double iMultiple;

   private double iSecondNumber;

   public NoteLength(double multiple, double firstNumber, double secondNumber)
   {
      iMultiple = multiple;
      iFirstNumber = firstNumber;
      iSecondNumber = secondNumber;
   }

   public NoteLength(NoteLength original)
   {
      iMultiple = original.getMultiple();
      iFirstNumber = original.getFirstNumber();
      iSecondNumber = original.getSecondNumber();
   }

   @Override
   public int compare(NoteLength o1, NoteLength o2)
   {
      return new Double(o1.getQuote()).compareTo(new Double(o2.getQuote()));
   }

   public double getFirstNumber()
   {
      return iFirstNumber;
   }

   public double getMultiple()
   {
      return iMultiple;
   }

   public String getName()
   {
      return Math.round(iFirstNumber) + "/" + Math.round(iSecondNumber);
   }

   public double getQuote()
   {
      return iFirstNumber / iSecondNumber;
   }

   public double getSecondNumber()
   {
      return iSecondNumber;
   }

   public void halfMultiple()
   {
      iMultiple /= 2;
      if (iFirstNumber > 1)
      {
         iFirstNumber /= 2;
      }
      else
      {
         iSecondNumber *= 2;
      }
   }

   public void multiply(double factor)
   {
      iMultiple *= factor;
      iFirstNumber *= factor;
      recalcFirstAndSecondNumber();
   }

   private void recalcFirstAndSecondNumber()
   {
      while (iFirstNumber % 2 == 0 && iSecondNumber % 2 == 0)
      {
         // Both can be divided with two
         iFirstNumber /= 2;
         iSecondNumber /= 2;
      }
   }

   public void setFirstNumber(double firstNumber)
   {
      iFirstNumber = firstNumber;
   }

   public void setMultiple(double multiple)
   {
      iMultiple = multiple;
   }

   public void setSecondNumber(double secondNumber)
   {
      iSecondNumber = secondNumber;
   }

   @Override
   public String toString()
   {
      return "Best Note Length (millisec): " + iMultiple + " Name: "
            + getName();
   }

}
