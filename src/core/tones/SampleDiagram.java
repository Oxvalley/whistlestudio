package core.tones;

import java.awt.Color;
import java.awt.Graphics;

import common.ui.components.ext.JPanelExt;
import core.wav.record.BufferContainer;

public class SampleDiagram extends JPanelExt
{

   /**
	 * 
	 */
   private static final long serialVersionUID = -4493631308738350114L;
   private BufferContainer iBufferContainer;
   private double iSampleTime = 0.0;

   public SampleDiagram(int width, int height)
   {
      super(width, height);
      setBackground(Color.GRAY);
   }

   public final void addBuffer(BufferContainer bufferContainer)
   {
      iBufferContainer = bufferContainer;
   }

   @Override
   public void paint(Graphics g)
   {
      super.paint(g);
      if (iBufferContainer != null && iBufferContainer.getBuffer() != null)
      {
         double xBlock =
               ((double) getWidth() / iBufferContainer.getBuffer().length);
         double yBlock = (getHeight() / 256.0);
         for (int i = 0; i < iBufferContainer.getBuffer().length; i++)
         {
            int value = iBufferContainer.getBuffer()[i];
            value = (value + 256) % 256;
            int x = (int) (xBlock * i);
            int y = (int) (yBlock * value);
            int y2 = (int) (yBlock * (256 - value));
            g.setColor(Color.YELLOW);
            g.drawLine(x, y, x, y2);
         }
         g.setColor(Color.WHITE);
         g.drawString(String.valueOf(iSampleTime), 10, 30);
      }
   }

   public final void setTime(double sampleTime)
   {
      iSampleTime = sampleTime;
   }

}
