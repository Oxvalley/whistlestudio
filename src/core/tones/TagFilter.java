package core.tones;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class TagFilter extends FileFilter
{

   private String iTag;

   public TagFilter(String tag)
   {
      iTag = tag;
   }

   @Override
   public final boolean accept(File f)
   {
      return (f.isDirectory() || f.getName().endsWith("." + iTag));
   }

   @Override
   public String getDescription()
   {
      return "." + iTag;
   }

}
