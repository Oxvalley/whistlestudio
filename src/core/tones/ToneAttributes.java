package core.tones;

public class ToneAttributes
{

   public int getYPos()
   {
      return iYPos;
   }
   public void setYPos(int yPos)
   {
      iYPos = yPos;
   }
   public SignEnum getSigns()
   {
      return iSigns;
   }
   public void setSigns(SignEnum signs)
   {
      iSigns = signs;
   }
   private int iYPos;
   private SignEnum iSigns;
   
}
