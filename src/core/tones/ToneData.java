package core.tones;

import core.wav.SliceData;

public class ToneData
{

   private int endIndex = -1;

   private int[] iArray;

   private float iDurationInMilliSeconds;

   private long iHertz;

   private boolean iInTone;

   private Note iNote;
   private NoteLength iNoteLength = null;
   private int iPlayedNoteIndex = -1;
   private float iSampleRate;
   private ToneData iSilentAfterwards = null;
   private int iVelocity = 110;
   private int startIndex = Integer.MAX_VALUE;

   private int startTick;

   private int tickLength;

   private int iSampleBits;

   public ToneData(SliceData slice, float sampleRate, int sampleBits)
   {
      iSampleBits = sampleBits;
      iInTone = slice.isTone();
      iSampleRate = sampleRate;
      setIndexes(slice);
   }

   public ToneData(String toneName)
   {
      iNoteLength = new NoteLength(800,1.0,2.0);
      iInTone = true;
      iNote = new Note(toneName, -1);
   }

   public final void add(SliceData slice)
   {
      setIndexes(slice);
   }

   public final void calcNoteLength()
   {
      double milliDuration = iDurationInMilliSeconds;

      // Find best Note length, Start with max length 8 seconds for 4/1 note. (1/8 note = 250 ms)
      // used with default bpm 120 according to:
      // http://tomhess.net/Tools/DelayCalculator.aspx

      double multiple = 8000;
      double firstNumber = 4;
      double secondNumber = 1;

      if (iNoteLength == null)
      {
         iNoteLength = new NoteLength(multiple, firstNumber, secondNumber);
      }
      else
      {
         iNoteLength.setMultiple(multiple);
         iNoteLength.setFirstNumber(firstNumber);
         iNoteLength.setSecondNumber(secondNumber);
      }

      while (iNoteLength.getMultiple() > milliDuration
            && iNoteLength.getMultiple() > 25)
      {
         iNoteLength.halfMultiple();
      }

      if (iNoteLength.getMultiple() == 25)
      {
         return;
      }

      // We also use half-steps
      iNoteLength.halfMultiple();

      // How many half steps?
      int halfsteps =
            (int) Math.round(milliDuration / iNoteLength.getMultiple());
      iNoteLength.multiply(halfsteps);
      setNoteLength(iNoteLength);
   }

   private void calculateDurations()
   {
      iDurationInMilliSeconds = (endIndex - startIndex) * getTimeFactor();
   }

   public final float getTimeFactor()
   {
      return 8000f/(iSampleRate * iSampleBits);
   }

   public final int[] getArray()
   {
      return iArray;
   }

   public final float getDurationInMilliSeconds()
   {
      return iDurationInMilliSeconds;
   }

   public final int getEndIndex()
   {
      return endIndex;
   }

   public final long getHertz()
   {
      return iHertz;
   }

   public final Note getNote()
   {
      if (iNote == null)
      {
         iNote = new Note(null, -1);
      }

      return iNote;
   }

   public final NoteLength getNoteLength()
   {
      return iNoteLength;
   }

   public final int getPlayedToneIndex()
   {
      return iPlayedNoteIndex;
   }

   public final float getSampleRate()
   {
      return iSampleRate;
   }

   public final ToneData getSilentAfterwards()
   {
      return iSilentAfterwards;
   }

   public final int getStartIndex()
   {
      return startIndex;
   }

   public final int getVelocity()
   {
      return iVelocity;
   }

   public final boolean isTone()
   {
      return iInTone;
   }

   public final void setArray(int[] array)
   {
      iArray = array;
   }

   public final void setDurationInMilliSeconds(float durationInMilliSeconds)
   {
      iDurationInMilliSeconds = durationInMilliSeconds;
   }

   public final void setEndIndex(int endIndex2)
   {
      this.endIndex = endIndex2;
   }

   public final void setHertz(long hertz)
   {
      iHertz = hertz;
   }

   private void setIndexes(SliceData slice)
   {
      if (startIndex > slice.getStartIndex())
      {
         startIndex = slice.getStartIndex();
      }
      if (endIndex < slice.getEndIndex())
      {
         endIndex = slice.getEndIndex();
      }
      calculateDurations();
   }

   public final void setIsNoTone()
   {
      iInTone = false;
   }

   public final void setNote(Note note)
   {
      iNote = note;
   }

   private void setNoteLength(NoteLength noteL)
   {
      iNoteLength = noteL;
   }

   public void setPlayedNoteIndex(int index)
   {
      iPlayedNoteIndex = index;

   }

   public void setSampleRate(float sampleRate)
   {
      iSampleRate = sampleRate;
   }

   public void setSilentAfterwards(ToneData toneData)
   {
      iSilentAfterwards = toneData;
   }

   public void setStartIndex(int startIndex2)
   {
      this.startIndex = startIndex2;
   }

   @Override
   public String toString()
   {
      String retVal =
            "Tone start " + getStartIndex() + " end " + getEndIndex()
                  + " Duration(ms): " + iDurationInMilliSeconds + " ";

      if (iInTone)
      {
         retVal += "Hertz: " + getHertz() + " Best fit: ";
      }

      if (getNote() != null)
      {
         retVal += getNote().toString();
      }

      if (iNoteLength != null)
      {
         retVal += " NoteLength: " + iNoteLength.toString();
      }

      if (iSilentAfterwards != null)
      {
         retVal += " Silent afterwards: (" + iSilentAfterwards.toString() + ")";
      }


      return retVal;
   }

   public void setStartTick(int startTick2)
   {
      this.startTick = startTick2;
   }

   public void setTickLength(int tickLength2)
   {
      this.tickLength = tickLength2;
   }

   public int getStartTick()
   {
      return startTick;
   }

   public int getTickLength()
   {
      return tickLength;
   }
}
