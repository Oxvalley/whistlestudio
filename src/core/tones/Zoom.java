package core.tones;

public class Zoom
{

   public static double getSize(double zoom, double length)
   {
      return zoom / 100.0 * length;
   }

   public static int getSizeInt(double zoom, int length)
   {
      return (int) Math.round(getSize(zoom, length));
   }

}
