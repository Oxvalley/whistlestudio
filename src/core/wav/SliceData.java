package core.wav;

import core.tones.FastFourierTransform;

public class SliceData
{

   private final long iAverage;

   private final long iDiff;

   private final int iEndIndex;

   private final int iStartIndex;
   private boolean isTone;

   private float[] iPartArr;

   private long iFreq = -1;

   private float iSampleRate;

   private int iMinHertz;

   private int iMaxHertz;

   public SliceData(int startIndex, int endIndex, long average, long diff, float[] part, float sampleRate, int minHertz, int maxHertz)
   {
      iStartIndex = startIndex;
      iEndIndex = endIndex;
      iAverage = average;
      iDiff = diff;
      iPartArr = part;
      iSampleRate = sampleRate;
      iMinHertz = minHertz;
      iMaxHertz = maxHertz;
   }

   public long getAverage()
   {
      return iAverage;
   }

   public long getDiff()
   {
      return iDiff;
   }

   public int getEndIndex()
   {
      return iEndIndex;
   }

   public int getStartIndex()
   {
      return iStartIndex;
   }

   public boolean isTone()
   {
      return isTone;
   }

   public void setTone(boolean isTone2)
   {
      this.isTone = isTone2;
   }

   public boolean isToneFrequence()
   {
      if (iFreq == -1)
      {
         iFreq=     
               FastFourierTransform.getHertz(iPartArr,
                     iSampleRate, iMinHertz, iMaxHertz);
         System.out.println(iStartIndex + " " + iFreq);

      }
      
       return (iFreq > 1200);
   }

}
