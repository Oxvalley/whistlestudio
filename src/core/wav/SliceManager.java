package core.wav;

import java.awt.Point;
import java.util.List;

import org.jfree.data.xy.XYSeries;

public class SliceManager
{
   private List<SliceData> iDerives;
   // Not used, high and low yet
   private double iHigh;
   private double iLow;
   private int INoToneLevel;

   public SliceManager(List<SliceData> derives, double high, double low, int noToneLevel)
   {
      iDerives = derives;
      iHigh = high;
      iLow = low;
      INoToneLevel = noToneLevel;
   }

   public XYSeries calcTonesToXYSeries(String title, boolean isLimitReversed)
   {
      XYSeries serie = new XYSeries(title);

      for (SliceData slice : iDerives)
      {
         if ((isLimitReversed && slice.getAverage() < iHigh)
               || (!isLimitReversed && slice.getAverage() > iHigh))
         {
            slice.setTone(true);
         }
         else
         {
            slice.setTone(false);
         }

      }
              
      // Rule.. no one can have one state (Tone/NoTone) if it is less than iLow
      // in a row

      int startindex = -1;
      boolean isTone = false;
      for (int i = 0; i < iDerives.size(); i++)
      {
         SliceData slice = iDerives.get(i);
         if (startindex == -1)
         {
            startindex = i;
            isTone = slice.isTone();
         }

         if (!slice.isTone() == isTone)
         {
            // Tone changed
            int endIndex = i;
            isTone = slice.isTone();

            if (endIndex - startindex < iLow)
            {
               // To few in a row before this, change values to this
               for (int j = startindex; j < endIndex; j++)
               {
                  SliceData slice2 = iDerives.get(j);
                  slice2.setTone(isTone);
               }
            }
            startindex = i;
         }
      }

      for (SliceData slice : iDerives)
      {
         if (slice.isTone())
         {
            serie.add(slice.getStartIndex(), iHigh);
         }
         else
         {
            serie.add(slice.getStartIndex(),INoToneLevel);
         }
      }

      return serie;

   }
   
   public Point calcLowestHighest()
   {

      int lowest = Integer.MAX_VALUE;
      int highest = Integer.MIN_VALUE;
            
      for (SliceData slice : iDerives)
      {
         
         long average = slice.getAverage();
         lowest = (int) Math.min(lowest, average);
         highest = (int) Math.max(highest, average);
      }
      
      return new Point(lowest,highest);
}


}
