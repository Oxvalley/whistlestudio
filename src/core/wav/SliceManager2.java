package core.wav;

import java.util.List;

import org.jfree.data.xy.XYSeries;

public class SliceManager2
{
   private List<SliceData> iDerives;
   // Not used, high and low yet
   private double iHigh;
   private double iLow;
   private float iSampleRate;

   public SliceManager2(List<SliceData> derives, double high, double low, float sampleRate)
   {
      iDerives = derives;
      iHigh = high;
      iLow = low;
      iSampleRate = sampleRate;
   }

   public XYSeries calcTonesToXYSeries(String title)
   {
      XYSeries serie = new XYSeries(title);

      for (SliceData slice : iDerives)
      {
            slice.setTone(slice.isToneFrequence());
      }

      // Rule.. no one can have one state (Tone/NoTone) if it is less than iLow
      // in a row

      int startindex = -1;
      boolean isTone = false;
      for (int i = 0; i < iDerives.size(); i++)
      {
         SliceData slice = iDerives.get(i);
         if (startindex == -1)
         {
            startindex = i;
            isTone = slice.isTone();
         }

         if (!slice.isTone() == isTone)
         {
            // Tone changed
            int endIndex = i;
            isTone = slice.isTone();

            if (endIndex - startindex < iLow)
            {
               // To few in a row before this, change values to this
               for (int j = startindex; j < endIndex; j++)
               {
                  SliceData slice2 = iDerives.get(j);
                  slice2.setTone(isTone);
               }
            }
            startindex = i;
         }
      }

      for (SliceData slice : iDerives)
      {
         if (slice.isTone())
         {
            serie.add(slice.getStartIndex(), iHigh);
         }
         else
         {
            serie.add(slice.getStartIndex(), 8);
         }
      }

      return serie;

   }

}
