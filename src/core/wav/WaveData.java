package core.wav;

import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.swing.JProgressBar;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import core.tones.ToneData;
import ui.main.WaveProperties;

public final class WaveData
{
   private static byte[] abData;
   private static long averageCount;
   private static long averageSum;
   private static List<SliceData> iDerives;
   private static long iMiddle;
   private static float sample_rate;
   private static XYSeries series2 = null;
   private static int sample_bits;

   private WaveData()
   {

   }

   public static int[] getDataArray(ToneData toneData, boolean powerOf2,
         float percentCutBefore)
   {
      int size = toneData.getEndIndex() - toneData.getStartIndex();
      int newSize = size;
      if (powerOf2)
      {
         newSize = powerOf2LessThan(size);
      }

      int[] arr = new int[newSize];
      int j = 0;
      int diff = size - newSize;
      // In the middle of the note
      int start =
            (int) (toneData.getStartIndex() + (diff * percentCutBefore / 100));
      int end = start + newSize;

      for (int i = start; i < end; i++)
      {
         arr[j++] = abData[i];
      }
      return arr;
   }

   public static List<SliceData> getDerives()
   {
      return iDerives;
   }

   public static double getMiddle()
   {
      return iMiddle;
   }

   public static float getSampleRate()
   {
      return sample_rate;
   }

   public static XYSeries getSeries2()
   {
      return series2;
   }

   public static XYSeriesCollection getWaveDataset(String filename,
         JProgressBar prog)
   {
      // Get the location of the sound file
      File soundFile = new File(filename);
      WaveProperties wp = WaveProperties.getInstance();


      int fileBytesTotal = (int) soundFile.length();
      int byteCount = 0;
      if (prog != null)
      {
         prog.setMaximum(fileBytesTotal);
         prog.setMinimum(0);
         prog.setValue(byteCount);
      }

      int bufferSize = (int) soundFile.length();

      iDerives = new ArrayList<SliceData>();

      // Load the Audio Input Stream from the file
      AudioInputStream audioInputStream = null;
      try
      {
         audioInputStream = AudioSystem.getAudioInputStream(soundFile);
      }
      catch (Exception e)
      {
         System.out.println("Error reading file " + filename);
         return null;
      }

      // Get Audio Format information
      AudioFormat audioFormat = audioInputStream.getFormat();

      // Handle opening the line
      SourceDataLine line = null;
      DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
      try
      {
         line = (SourceDataLine) AudioSystem.getLine(info);
         line.open(audioFormat);
      }
      catch (LineUnavailableException e)
      {
         e.printStackTrace();
         return null;
      }
      catch (Exception e)
      {
         e.printStackTrace();
         return null;
      }

      // Write the sound to an array of bytes
      int nBytesRead = 0;
      int index = 0;
      long sum = 0;
      long numberCounter = 0;
      averageSum = 0;
      averageCount = 0;
      abData = new byte[bufferSize];
      series2 = new XYSeries("wavefile");
      long oldaverage = 0;
      sample_rate = audioFormat.getSampleRate();
      sample_bits = audioFormat.getSampleSizeInBits();

      while (nBytesRead != -1)
      {
         try
         {
            nBytesRead = audioInputStream.read(abData, 0, abData.length);

         }
         catch (IOException e)
         {
            e.printStackTrace();
         }

         if (nBytesRead >= 0)
         {

            for (int i = 0 + index; i < nBytesRead + index; i++)
            {
               byteCount++;

               if (numberCounter == wp.getBytesInSlice())
               {
                  float[] part = new float[wp.getBytesInSlice()];

                  arraycopy(abData, i - wp.getBytesInSlice(), part, 0, wp.getBytesInSlice());
                  if (prog != null)
                  {
                     prog.setValue(byteCount);
                  }
                  long average = sum / wp.getBytesInSlice();
                  averageSum += average;
                  averageCount++;
                  series2.add(i - wp.getBytesInSlice(), average);
                  long diff = average - oldaverage;
                  int minHertz = wp.getMinHertz();
                  int maxHertz = wp.getMaxHertz();

                  iDerives.add(new SliceData(i - wp.getBytesInSlice(), i, average, diff,
                        part, getSampleRate(), minHertz, maxHertz));
                  oldaverage = average;
                  sum = 0;
                  numberCounter = 0;
               }
               else
               {
                  // Get amplitude
                  int value = 128 - Math.abs(abData[i]);
                  sum += value;
                  numberCounter++;
               }

            }
         }
         index += nBytesRead;
      }

      // close the line
      line.drain();
      line.close();

      final XYSeriesCollection dataset = new XYSeriesCollection();
      dataset.addSeries(series2);

      if (prog != null)
      {
         prog.setValue(fileBytesTotal);
      }

      System.out.println("Supposed middle: " + averageSum / averageCount);

      iMiddle = averageSum / averageCount;

      return dataset;

   }

   /**
    *
    * @param abData2
    * @param index
    * @param part
    * @param i
    * @param nBytesRead
    */

   private static void arraycopy(byte[] inArray, int inStartIndex,
         float[] outArray, int outStartIndex, int copyCount)
   {
      int copyCount2 = copyCount;
      copyCount2 = Math.min(copyCount2, inArray.length - inStartIndex);
      copyCount2 = Math.min(copyCount2, outArray.length - outStartIndex);
      for (int i = 0; i < copyCount2; i++)
      {
         outArray[i + outStartIndex] = inArray[inStartIndex + i];
      }
   }

   private static int powerOf2LessThan(int size)
   {
      int i = 1;
      while (i < size)
      {
         i = i * 2;
      }
      return i / 2;
   }

   public static Point showSlices(XYSeriesCollection mDataset, int volLimit,
         int noVolLimit, boolean isLimitReveresed, double noMinSlices)
   {
      SliceManager dm = new SliceManager(WaveData.getDerives(), volLimit,
            noMinSlices, noVolLimit);

      String label = "";
      if (isLimitReveresed)
      {
         label = "R";
      }
      if (volLimit < noVolLimit)
      {
         label += volLimit + "-" + noVolLimit;
      }
      else
      {
         label += noVolLimit + "-" + volLimit;
      }
      XYSeries tonesSeries = dm.calcTonesToXYSeries(label, isLimitReveresed);

      if (tonesSeries != null)
      {
         mDataset.addSeries(tonesSeries);
      }

      return dm.calcLowestHighest();
   }

   public static void showSlices(XYSeriesCollection mDataset)
   {
      WaveProperties wp = WaveProperties.getInstance();
      SliceManager dm =
            new SliceManager(WaveData.getDerives(), wp.getToneVolLimit(),
                  wp.getMinSlices(), wp.getNoToneLimit());
      XYSeries tonesSeries = dm.calcTonesToXYSeries("Tones",
            wp.isVolumeLimitReverse());

      if (tonesSeries != null)
      {
         mDataset.removeAllSeries();

         mDataset.addSeries(WaveData.getSeries2());
         mDataset.addSeries(tonesSeries);
      }
   }

   public static int getSamplebits()
   {
      return sample_bits;
   }

}
