package core.wav;

import java.io.File;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;

public class WavePlayer extends Thread
{
   public static void playWave(String filename)
   {
      /*
       * This code is based on the example found at:
       * http://www.jsresources.org/examples/ClipPlayer.java.html
       */
      // Load the sound file
      File soundFile = new File(filename);

      // Object that will hold the audio input stream for the sound
      Object currentSound = null;

      // Object that will play the audio input stream of the sound
      Clip clip = null;

      // Object that contains format information about the audio input stream
      AudioFormat format = null;

      // Load the audio input stream
      try
      {
         currentSound = AudioSystem.getAudioInputStream(soundFile);
      }
      catch (Exception e1)
      {
         System.out.println("Error loading file");
      }

      try
      {
         // Get the format information from the Audio Input Stream
         AudioInputStream stream = (AudioInputStream) currentSound;
         format = stream.getFormat();

         // Get information about the line
         DataLine.Info info =
               new DataLine.Info(Clip.class, stream.getFormat(),
                     ((int) stream.getFrameLength() * format.getFrameSize()));

         // Load clip information from the line information
         clip = (Clip) AudioSystem.getLine(info);

         // Write the stream onto the clip
         clip.open(stream);

         // make the current sound the clip
         currentSound = clip;

         // start the clip
         clip.start();
      }
      catch (Exception ex)
      {
         ex.printStackTrace();
         currentSound = null;
         return;
      }

      // specify a new sound level from 0 to 100
      double gain = 80.0;

      // create the gain control object using the clip object from the code
      // above
      FloatControl control =
            (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);

      // convert the given sound level to decibel
      gain = gain / 100;
      if (gain == 0.0)
      {
         gain = 0.0001;
      }
      float dB = (float) (Math.log(gain) / Math.log(10.0) * 20.0);

      // change the sound level
      control.setValue(dB);

   }

   private Clip clip;
   private String iFileName;

   private AudioInputStream stream;

   public WavePlayer(String waveFileName)
   {
      iFileName = waveFileName;
   }

   public Clip getClip()
   {
      return clip;
   }

   public long getMicrosecondLength()
   {
      return (long) ((stream.getFrameLength() / stream.getFormat()
            .getSampleRate()) * 1000);
   }

   public boolean isRunning()
   {
      return clip.isRunning();
   }

   @Override
   public void run()
   {
      // Load the sound file
      File soundFile = new File(iFileName);

      // Object that will hold the audio input stream for the sound
      Object currentSound = null;

      // Object that will play the audio input stream of the sound
      clip = null;

      // Object that contains format information about the audio input stream
      AudioFormat format = null;

      // Load the audio input stream
      try
      {
         currentSound = AudioSystem.getAudioInputStream(soundFile);
      }
      catch (Exception e1)
      {
         System.out.println("Error loading file " + iFileName + "\n"
               + e1.getMessage());
         return;
      }

      try
      {
         // Get the format information from the Audio Input Stream
         stream = (AudioInputStream) currentSound;
         format = stream.getFormat();

         // Get information about the line
         DataLine.Info info =
               new DataLine.Info(Clip.class, stream.getFormat(),
                     ((int) stream.getFrameLength() * format.getFrameSize()));

         // Load clip information from the line information
         clip = (Clip) AudioSystem.getLine(info);

         // Write the stream onto the clip
         clip.open(stream);

         // make the current sound the clip
         currentSound = clip;
         System.out.println("Playing wave file");
         // start the clip
         clip.start();
      }
      catch (Exception ex)
      {
         ex.printStackTrace();
         currentSound = null;
         return;
      }

      // specify a new sound level from 0 to 100
      double gain = 80.0;

      // create the gain control object using the clip object from the code
      // above
      FloatControl control =
            (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);

      // convert the given sound level to decibel
      gain = gain / 100;
      if (gain == 0.0)
      {
         gain = 0.0001;
      }
      float dB = (float) (Math.log(gain) / Math.log(10.0) * 20.0);

      // change the sound level
      control.setValue(dB);

   }

   public void stopPlaying()
   {
      clip.stop();
      clip.close();
   }

}
