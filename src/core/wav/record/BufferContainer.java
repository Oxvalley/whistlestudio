package core.wav.record;

public class BufferContainer
{

   private final byte[] iBuffer;

   public BufferContainer(byte[] buffer)
   {
      iBuffer = buffer;
   }

   public byte[] getBuffer()
   {
      return iBuffer;
   }

}
