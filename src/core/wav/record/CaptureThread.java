package core.wav.record;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.TargetDataLine;
import javax.swing.JOptionPane;

import ui.main.WaveProperties;

public class CaptureThread extends Thread
{

   private AudioFormat audioFormat;
   private int bufferSize;
   private final String fileName;
   private List<ActionListener> listeners = new ArrayList<ActionListener>();
   private ShowCaptureThread showCapture;
   private TargetDataLine targetDataLine;

   public CaptureThread(String fileName2)
   {
      this.fileName = fileName2;
   }

   public final void addActionListener(ActionListener listener)
   {
      listeners.add(listener);
   }

   public void captureAudio()
   {
      WaveProperties wp = WaveProperties.getInstance();
      Mixer.Info[] aInfos = null;
      int driverIndex = wp.getDriver();
      try
      {
         audioFormat = getAudioFormat();
         bufferSize =
               (int) audioFormat.getSampleRate() * audioFormat.getFrameSize();

         aInfos = AudioSystem.getMixerInfo();

         if (driverIndex == -1)
         {
            DataLine.Info dataLineInfo =
                  new DataLine.Info(TargetDataLine.class, audioFormat);
            targetDataLine = (TargetDataLine) AudioSystem.getLine(dataLineInfo);
         }
         else
         {
            // Use line below to switch to another line in
            targetDataLine =
                  AudioSystem.getTargetDataLine(audioFormat,
                        aInfos[driverIndex]);
         }

         start();

      }
      catch (Exception e)
      {
         if (e.getMessage().contains("Line unsupported:"))
         {
            JOptionPane.showConfirmDialog(null,
                  "Driver " + aInfos[driverIndex].getName()
                        + " for sound input is not supported. "
                        + "Chose a better one in options");
            // TODO Tell listeners we are done now
            ActionEvent action = new ActionEvent(this, -1, null);
            action.setSource(new BufferContainer(null));
            for (ActionListener listener : listeners)
            {
               listener.actionPerformed(action);
            }

            return;
         }
         e.printStackTrace();
      } // end catch
   } // end captureAudio method

   private static AudioFormat getAudioFormat()
   {
      WaveProperties wp = WaveProperties.getInstance();
      float sampleRate = Float.valueOf(wp.getSampleRate()).floatValue();
      int sampleSizeInBits = wp.getSampleBits();
      int channels = 1;
      boolean signed = false;
      boolean bigEndian = false;
      return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed,
            bigEndian);
   } // end getAudioFormat

   private void getRecordedData(final TargetDataLine line)
   {
      showCapture = new ShowCaptureThread(line, bufferSize);
      showCapture.addActionListeners(listeners);
      showCapture.start();

   }

   @Override
   public void run()
   {

      try
      {
         targetDataLine.open(audioFormat);
         targetDataLine.start();
         getRecordedData(targetDataLine);
      }
      catch (Exception e)
      {
         e.printStackTrace();
      } // end catch
   } // end run

   public void stopRecording()
   {
      if (targetDataLine != null)
      {
         targetDataLine.stop();
         targetDataLine.close();
      }

      showCapture.stopRunning();
      // Tell all listeners to stop listening
      showCapture.fireActionEvent();

      AudioFileFormat.Type fileType = null;
      File audioFile = null;
      try
      {
         fileType = AudioFileFormat.Type.WAVE;
         audioFile = new File(fileName);
         AudioSystem.write(new AudioInputStream(new ByteArrayInputStream(
               showCapture.getCaptured().toByteArray()), getAudioFormat(),
               showCapture.getCaptured().toByteArray().length), fileType,
               audioFile);
      }
      catch (FileNotFoundException e)
      {
         e.printStackTrace();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }

   }

} // end inner class CaptureThread
