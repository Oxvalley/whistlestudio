package core.wav.record;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.TargetDataLine;

public class ShowCaptureThread extends Thread
{

   private byte[] buffer;
   private final TargetDataLine iLine;
   private List<ActionListener> listeners = new ArrayList<ActionListener>();
   private ByteArrayOutputStream out;
   private boolean running;

   public ShowCaptureThread(TargetDataLine line, int bufferSize)
   {
      iLine = line;
      buffer = new byte[bufferSize];
   }

   public final void addActionListener(ActionListener listener)
   {
      listeners.add(listener);
   }

   public final void addActionListeners(List<ActionListener> listeners2)
   {
      listeners.addAll(listeners2);
   }

   final void fireActionEvent()
   {
      ActionEvent action = new ActionEvent(this, -1, null);
      action.setSource(new BufferContainer(buffer));
      for (ActionListener listener : listeners)
      {
         listener.actionPerformed(action);
      }

   }

   public final ByteArrayOutputStream getCaptured()
   {
      return out;
   }

   @Override
   public void run()
   {
      out = new ByteArrayOutputStream();
      running = true;
      try
      {
         while (running)
         {
            if (buffer != null)
            {
               int count = iLine.read(buffer, 0, buffer.length);
               if (count > 0)
               {
                  fireActionEvent();
                  out.write(buffer, 0, count);
               }
            }
         }
         out.close();
      }
      catch (IOException e)
      {
         System.err.println("I/O problems: " + e);
         System.exit(-1);
      }
   }

   public void stopRunning()
   {
      running = false;
   }

}
