package diagrams;

import java.awt.Color;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;

public final class Charter
{
   private Charter()
   {
   }

   public static JFreeChart createChart(final XYDataset dataset, String title)
   {

      // create the chart...
      final JFreeChart chart = ChartFactory.createXYLineChart(title, // chart
            // title
            "Tid", // x axis label
            "Amplitude", // y axis label
            dataset, // data
            PlotOrientation.VERTICAL, true, // include legend
            true, // tooltips
            false);

      // NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
      chart.setBackgroundPaint(Color.white);

      // get a reference to the plot for further customisation...
      final XYPlot plot = chart.getXYPlot();
      plot.setBackgroundPaint(Color.lightGray);
      plot.setDomainGridlinePaint(Color.white);
      plot.setRangeGridlinePaint(Color.white);

      final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
      renderer.setSeriesLinesVisible(1, true);
      renderer.setSeriesLinesVisible(0, true);
      renderer.setSeriesShapesVisible(0, false);
      renderer.setSeriesShapesVisible(1, false);
      renderer.setSeriesPaint(0, Color.ORANGE);
      renderer.setSeriesPaint(1, Color.BLUE);

      // change the auto tick unit selection to integer units only...
      final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
      rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
      // OPTIONAL CUSTOMISATION COMPLETED.

      return chart;

   }
}
