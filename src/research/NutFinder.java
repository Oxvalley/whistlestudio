package research;

public final class NutFinder
{

   public static void main(String[] args)
   {
      
      
      
      for (int i = 111; i < 1000; i++)
      {
         String num = String.valueOf(i);
         String a = num.substring(0, 1);
         String b = num.substring(1, 2);
         String c = num.substring(2, 3);

         if (a != b && a != c && b != c)
         {
            String number1 = a + b;
            String number2 = a + c;
            String number3 = b + a;
            String number4 = b + c;
            String number5 = c + a;
            String number6 = c + b;

            int sum =
                  Integer.valueOf(number1) + Integer.valueOf(number2)
                        + Integer.valueOf(number3) + Integer.valueOf(number4)
                        + Integer.valueOf(number5) + Integer.valueOf(number6);
            if (sum == i * 2)
            {
               System.out.println("Hittat: " + i);
               System.out.println(number1 + " + " + number2 + " + " + number3
                     + " + " + number4 + " + " + number5 + " + " + number6
                     + " = " + sum + " = " + i + " * 2");
            }
         }

      }

   }

   private NutFinder()
   {
      super();
   }
}
