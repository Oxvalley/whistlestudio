package research;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class TestWB extends JFrame
{

   /**
    * 
    */
   private static final long serialVersionUID = 8104331120173942136L;
   private JPanel contentPane;
   private JTextField iTextField;
   private final ButtonGroup buttonGroup = new ButtonGroup();

   /**
    * Launch the application.
    */
   public static void main(String[] args)
   {
      EventQueue.invokeLater(new Runnable()
      {
         @Override
         public void run()
         {
            try
            {
               TestWB frame = new TestWB();
               frame.setVisible(true);
            }
            catch (Exception e)
            {
               e.printStackTrace();
            }
         }
      });
   }

   /**
    * Create the frame.
    */
   public TestWB()
   {
      setIconImage(Toolkit
            .getDefaultToolkit()
            .getImage(
                  TestWB.class
                        .getResource("/com/sun/java/swing/plaf/windows/icons/Inform.gif")));
      setResizable(false);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setBounds(100, 100, 317, 233);
      contentPane = new JPanel();
      contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
      setContentPane(contentPane);
      contentPane.setLayout(new BorderLayout(8, 5));

      JPanel panel = new JPanel();
      contentPane.add(panel);
      panel.setLayout(null);

      JLabel lblName = new JLabel("Text:");
      lblName.setBounds(9, 3, 86, 19);
      panel.add(lblName);

      iTextField = new JTextField();
      iTextField.setBounds(43, 2, 216, 23);
      panel.add(iTextField);
      iTextField.setColumns(10);

      JPanel panel_2 = new JPanel();
      panel_2.setBounds(43, 135, 216, 24);
      panel.add(panel_2);
      panel_2.setLayout(null);
      setTitle("My First Window");

      JButton btnOk = new JButton("OK");
      btnOk.setBounds(0, 0, 89, 23);
      panel_2.add(btnOk);

      JButton button = new JButton("Cancel");
      button.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent arg0)
         {
            System.out.println("Cancelled!!");
         }
      });
      button.setBounds(125, 0, 89, 23);
      panel_2.add(button);
      btnOk.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent arg0)
         {
            System.out.println(iTextField.getText());
         }
      });

      JPanel panel_1 = new JPanel();
      panel_1.setBounds(43, 30, 216, 83);
      panel.add(panel_1);
      panel_1.setLayout(null);

      JRadioButton rdbtnEtt = new JRadioButton("Ett");
      buttonGroup.add(rdbtnEtt);
      rdbtnEtt.setBounds(83, 3, 49, 23);
      panel_1.add(rdbtnEtt);

      JRadioButton rdbtnTv = new JRadioButton("Tv\u00E5");
      buttonGroup.add(rdbtnTv);
      rdbtnTv.setBounds(83, 29, 49, 23);
      panel_1.add(rdbtnTv);

      JRadioButton rdbtnTre = new JRadioButton("Tre");
      buttonGroup.add(rdbtnTre);
      rdbtnTre.setBounds(83, 55, 49, 23);
      panel_1.add(rdbtnTre);
      // panel_1.setFocusTraversalPolicy(new FocusTraversalOnArray(
      // new Component[] { rdbtnEtt, rdbtnTv, rdbtnTre }));
   }
}
