package ui.main;

import java.awt.Point;
import java.util.List;

import javax.swing.JOptionPane;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import common.ui.components.ProgressWindow;
import core.tones.FrequenceFinder;
import core.tones.Note;
import core.tones.ToneData;
import core.wav.WaveData;

public class AutoConfig implements Runnable
{

   private XYSeriesCollection iDataset;
   private String iWavFilename;
   private static final int MAX_WHISTLE_HERTZ = 5000;
   private static final int MIN_WHISTLE_HERTZ = 500;
   private WaveListener iWaveListener;

   public AutoConfig(String wavFilename, XYSeriesCollection dataset,
         WaveListener waveListener)
   {
      iWavFilename = wavFilename;
      iDataset = dataset;
      iWaveListener = waveListener;
   }

   @Override
   public void run()
   {
      initAutoConfig();
   }

   public void initAutoConfig()
   {

      XYSeries series = WaveData.getSeries2();
      WaveProperties wp = WaveProperties.getInstance();
      if (series != null)
      {

         Point levels =
               WaveData.showSlices(iDataset, 0, 0, false, wp.getMinSlices());
         iDataset.removeAllSeries();
         iDataset.addSeries(WaveData.getSeries2());
         ProgressWindow pw =
               new ProgressWindow("Analyzing.. Please wait...", 24, WhistleStudio.ICON_NAME);

         Thread thread = new Thread(pw);
         thread.start();
         int step = (int) Math.ceil((levels.y - levels.x) / 6.0);
         pw.print("Possible levels from " + levels.x + " to " + levels.y);

         ConfigResult crNoReverse = autoToneTest(levels, pw, step, false);
         ConfigResult crReverse = autoToneTest(levels, pw, step, true);

         pw.print("Sum Points not reversed: " + crNoReverse.getPointsSum());
         pw.print("Sum Points reversed: " + crReverse.getPointsSum());

         Point levels2 = new Point();
         boolean reverseLimit = false;
         ConfigResult bestCr = crNoReverse;
         if (crReverse.getPointsSum() > crNoReverse.getPointsSum())
         {
            reverseLimit = true;
            bestCr = crReverse;
         }

         if (reverseLimit)
         {
            pw.print("This config skould be reversed");
         }
         else
         {
            pw.print("This config skould not be reversed");
         }
         levels2.x = bestCr.getBestLevel() - step;
         levels2.y = bestCr.getBestLevel() + step;
         pw.print("Best Level: " + bestCr.getBestLevel());
         pw.print("Best Points: " + bestCr.getBestPoints());

         pw.print("Test 2");
         // Lets make a second test near best points
         step = (int) Math.ceil((levels2.y - levels2.x) / 10.0);
         ConfigResult cr2 = autoToneTest(levels2, pw, step, reverseLimit);

         if (cr2.getBestPoints() >= bestCr.getBestPoints())
         {
            bestCr = cr2;
         }

         // Hard coded so far
         String minNoteLength = "1/64";
         int minSlicesInARow = 2;
         int minHertz = 500;
         int maxHertz = 5000;

         pw.print("\nFinal Suggestion");
         pw.print("================");
         pw.print("Best Points: " + bestCr.getBestPoints());
         pw.print("- Song duration (s): " + bestCr.getDuration());
         pw.print("\nMin Note Length: " + minNoteLength);
         pw.print("Min Slices in a row: " + minSlicesInARow);
         pw.print("Min hertz: " + minHertz);
         pw.print("Max hertz: " + maxHertz);
         pw.print("Tone vol limit: " + bestCr.getBestLevel());
         String answer = "No";
         if (reverseLimit)
         {
            answer = "Yes";
         }
         pw.print("Reversed vol limit:: " + answer);

         pw.increaseProgressBar();

         int dialogResult = JOptionPane.showConfirmDialog(null,
               "Would you like to set options to these suggested settings?",
               "Question", JOptionPane.YES_NO_CANCEL_OPTION);
         if (dialogResult == JOptionPane.YES_OPTION)
         {
            wp.setTempo(WhistlePropertyConstants.DEFAULT_TEMPO);
            wp.setNoteLength(minNoteLength);
            wp.setMinSlices(minSlicesInARow);
            wp.setMinHertz(minHertz);
            wp.setMaxHertz(maxHertz);
            wp.setToneVolLimit(bestCr.getBestLevel());
            wp.setVolumeLimitReverse(reverseLimit);
            iWaveListener.initOptions(iWavFilename);
            iWaveListener.processFileNoPath(iWavFilename);
            pw.dispose();
         }

      }
   }

   public ConfigResult autoToneTest(Point levels, ProgressWindow pw, int step,
         boolean reverseVolLimit)
   {
      ConfigResult cr = new ConfigResult();
      WaveProperties wp = WaveProperties.getInstance();
      int pointsSum = 0;

      String text = "Testing level ";
      if (reverseVolLimit)
      {
         text = "Reversing. " + text;
      }

      for (int thisLevel = levels.x; thisLevel <= levels.y; thisLevel += step)
      {
         pw.print("\n" + text + thisLevel);
         WaveData.showSlices(iDataset, thisLevel, thisLevel - step + 3,
               reverseVolLimit, wp.getMinSlices());
         FrequenceFinder ff = new FrequenceFinder(WaveData.getDerives());
         List<ToneData> iToneDataList =
               ff.createNoteList(MIN_WHISTLE_HERTZ, MAX_WHISTLE_HERTZ);
         int higestIndex = Integer.MIN_VALUE;
         int lowestIndex = Integer.MAX_VALUE;
         double higestHertz = Double.MIN_VALUE;
         double lowestHertz = Double.MAX_VALUE;
         int sumToneLengths = 0;
         int startindex = -1;
         int endindex = -1;
         double timefactor = -1;
         int noteHopps = 0;
         int lastIndex = -1;
         int numberOfTones = 0;

         for (ToneData td : iToneDataList)
         {
            Note note = td.getNote();
            int index = note.getMidiToneIndex();
            double hertz = note.getHertz();

            if (index > -1)
            {
               numberOfTones++;

               if (lastIndex != -1)
               {
                  noteHopps += Math.abs(lastIndex - index);
               }

               if (startindex == -1)
               {
                  // Start of first note
                  startindex = td.getStartIndex();
                  timefactor = td.getTimeFactor();
               }

               // End of last note
               endindex = td.getEndIndex();

               higestIndex = Math.max(higestIndex, index);
               lowestIndex = Math.min(lowestIndex, index);
               lastIndex = index;
               higestHertz = Math.max(higestHertz, hertz);
               lowestHertz = Math.min(lowestHertz, hertz);
               sumToneLengths += td.getDurationInMilliSeconds();
            }
         }

         double durationInMilliSeconds =
               (endindex - startindex) * timefactor / 1000.0;

         int averageToneLength = 0;
         if (numberOfTones > 0)
         {
            averageToneLength = sumToneLengths / numberOfTones;
         }

         pw.print("- Number of tones: " + numberOfTones);
         pw.print("- Tone spread: " + (higestIndex - lowestIndex));
         pw.print("- Average tone length (ms): " + averageToneLength);
         pw.print("- Note Hopps: " + noteHopps);
         pw.print("- Song duration (s): " + durationInMilliSeconds);
         pw.print("- Lowest Hertz: " + lowestHertz);
         pw.print("- Highest Hertz: " + higestHertz);

         int points = getPoints(durationInMilliSeconds, numberOfTones,
               (higestIndex - lowestIndex), averageToneLength, noteHopps,
               levels, thisLevel);
         pw.print("- Points: " + points);
         pointsSum += points;

         if (points > cr.getBestPoints())
         {
            cr.setBestPoints(points);
            cr.setBestLevel(thisLevel);
            cr.setLowestHertz(lowestHertz);
            cr.setHighestHertz(higestHertz);
            cr.setDuration(durationInMilliSeconds);
         }

         pw.increaseProgressBar();
      }

      cr.setPointsSum(pointsSum);
      return cr;
   }

   private static int getPoints(double durationInMilliSeconds, int noteCount,
         int spread, int toneLength, int noteHopps, Point levels, int thisLevel)
   {
      // Spread is most important
      int sum = 0;
      if (spread >= 2 && spread <= 3)
      {
         sum += 1000;
      }
      else if (spread >= 4 && spread <= 17)
      {
         sum += 2000;
      }
      else if (spread >= 18 && spread <= 21)
      {
         sum += 1000;
      }

      // Number of notes
      double notesASecond = durationInMilliSeconds / noteCount * 1.0;
      if (notesASecond >= 0.20 && notesASecond < 1.1)
      {
         sum += 400;
      }
      else if (notesASecond >= 1.1 && notesASecond <= 3.5)
      {
         sum += 300;
      }

      // Average tone length
      if (toneLength >= 120 && toneLength <= 220)
      {
         sum += 300;
      }
      else if (toneLength > 220 && toneLength <= 800)
      {
         sum += 200;
      }

      // Distance from min/max level... 30% above min is good
      double percent = 100f * Math.abs(thisLevel - levels.x)
            / Math.abs(levels.y - levels.x);
      sum += (Math.abs(30 - percent) * -600) + 7000;

      // Less note hopps is better
      if (noteCount > 0)
      {
         sum -= (noteHopps / noteCount * 300);
      }

      return sum;
   }

}
