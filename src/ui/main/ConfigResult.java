package ui.main;

public class ConfigResult
{

   private int iPointsSum;
   private int iBestPoints;
   private int iBestLevel;
   private double iLowestHertz;
   private double iHighestHertz;
   private double iDurationInMilliSeconds;

   public void setPointsSum(int pointsSum)
   {
      iPointsSum = pointsSum;
   }

   public int getBestPoints()
   {
      return iBestPoints;
   }

   public void setBestPoints(int points)
   {
      iBestPoints = points;
   }

   public void setBestLevel(int level)
   {
      iBestLevel = level;
   }

   public int getPointsSum()
   {
      return iPointsSum;
   }

   public int getBestLevel()
   {
      return iBestLevel;
   }

   public void setLowestHertz(double hertz)
   {
      iLowestHertz = hertz;
   }

   public void setHighestHertz(double hertz)
   {
      iHighestHertz = hertz;
   }

   public double getLowestHertz()
   {
      return iLowestHertz;
   }

   public double getHigestHertz()
   {
      return iHighestHertz;
   }

   public void setDuration(double durationInMilliSeconds)
   {
      iDurationInMilliSeconds = durationInMilliSeconds;
   }

   public double getDuration()
   {
      return iDurationInMilliSeconds;
   }

}
