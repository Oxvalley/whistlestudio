package ui.main;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JComboBox;

public class FileManager
{

   private String iFolderPath;
   private String iPostfix;
   private List<File> iFiles;

   public FileManager(String folderPath, String postfix)
   {
      iFolderPath = folderPath;
      iPostfix = postfix;
   }

   public List<String> getFilesAsStrings()
   {
      List<String> retList = new ArrayList<String>();
      List<File> list = getFiles();
      for (File file : list)
      {
         retList.add(file.getName());
      }

      return retList;
   }

   private List<File> getFiles()
   {
      return getFiles(false);
   }

   public String getFirstFilename(String defaultName) {
      List<String> files = getFilesAsStrings();
      if( files.size() == 0) {
         return defaultName;
      }
      else
      {
         return files.get(0);
      }
   }

   public List<File> getFiles(boolean refresh)
   {
      if (refresh || iFiles == null)
      {
         File file = new File(iFolderPath);
         File[] files = file.listFiles(new FileFilter()
         {

            @Override
            public boolean accept(File pathname)
            {
               if (iPostfix == null)
               {
                  return true;
               }
               else
               {
                  return pathname.getName().toLowerCase().endsWith(iPostfix);
               }
            }
         });
         iFiles = Arrays.asList(files);
      }

      return iFiles;
   }

   public void refreshFileCombo(JComboBox cmb)
   {
      cmb.removeAllItems();
      List<File> files = getFiles(true);

      for (File file : files)
      {
         cmb.addItem(file.getName());
      }
   }

   public String getFullPath(String fileName)
   {
      return iFolderPath+ File.separator+ fileName;
   }

}
