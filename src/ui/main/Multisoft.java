package ui.main;

public class Multisoft
{
   private final static int CONSTANT = 215;
   private final static int TABLE_ROWS = 2779950 / CONSTANT;

   public static void main(String[] args)
   {
      System.out.println(getIntFromSQLSimulator(447));

      long sum = 0;
      for ( int i = 0; i < 888; i++)
      {
         sum += getIntFromSQLSimulator(i);
      }

      System.out.println(sum);
   }

   private static long getIntFromSQLSimulator(int i)
   {

      if (i < 215)
      {
         return getCount(i) * i;
      }
      else
      {
         return getCount(i) * TABLE_ROWS;
      }

   }

   private static int getCount(int i)
   {
      if (i < 766)
      {
         return CONSTANT;
      }
      else
      {
         return 0;
      }
   }

}
