package ui.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.sound.midi.Instrument;
import javax.sound.midi.Soundbank;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Mixer;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import common.ui.components.ext.CompExtEnum;
import common.ui.components.ext.JPanelExt;
import common.utils.AppInfoInterface;
import common.utils.GraphicsUtil;
import core.midi.MidiCreator;
import core.tones.NoteLength;
import system.AppInfo;

public class OptionsFrame extends JFrame
{
   private static OptionsFrame iOptionsFrame;
   private JTextField txtMinSlices;
   private JTextField txtMinHertz;
   private JTextField txtMaxHertz;
   private JTextField txtToneVolLimit;
   private JCheckBox chkReverseMaxVol;
   private JComboBox cmbNoteLength;
   private JSlider sliderMinSlices;
   private JSlider sliderMinHertz;
   private JSlider sliderMaxHertz;
   private JSlider sliderToneVolLimit;
   private JComboBox cmbDriver;
   private JTextField txtPrefix;
   private JTextField txtOctave;
   private JTextField txtTone;
   private JSlider sliderTempo;
   private JTextField txtTempo;
   private JSlider sliderBytesInSlice;
   private JTextField txtBytesInSlice;
   private JTextField txtSampleRate;
   private JTextField txtSampleBits;
   private JTextField txtNoToneLimit;
   private JComboBox cmbInstruments;
   private WaveListener iWaveListener;
   private String iFileName;
   private JButton btnOctavePlus;
   private JButton btnTonePlus;

   public OptionsFrame(String fileName, WaveListener waveListener)
   {
      iWaveListener = waveListener;
      iFileName = fileName;
      AppInfoInterface appInfo = AppInfo.getInstance();
      setIconImage(getToolkit().getImage(WhistleStudio.ICON_NAME));
      setTitle("Options for " + fileName);

      int width = 600;
      int height = 620;

      JTabbedPane tabbedPane = new JTabbedPane();
      // ImageIcon icon = createImageIcon("images/middle.gif");

      JComponent panel1 = getFilePropertiesPanel(width, height);
      tabbedPane.addTab("File Properties", null, panel1, "File Properties");
      tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);

      JComponent panel2 = getGeneralPropertiesPanel(width, height);
      tabbedPane.addTab("General Properties", null, panel2,
            "General Properties");
      tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);

      getContentPane().add(tabbedPane);
      setSize(width, height);

      addWindowListener(new java.awt.event.WindowAdapter()
      {

         @Override
         public void windowClosing(java.awt.event.WindowEvent e)
         {
            setVisible(false);
         }
      });
   }

   private JComponent getGeneralPropertiesPanel(int width, int height)
   {
      JPanelExt panel = new JPanelExt(width, height);
      WaveProperties wp = WaveProperties.getInstance();
      // Sound input
      JLabel lblSoundInput = new JLabel("Sound input:");
      panel.addComponentExt(lblSoundInput, 10, 10, 100,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      cmbDriver = new JComboBox();
      Mixer.Info[] aInfos = AudioSystem.getMixerInfo();
      for (javax.sound.sampled.Mixer.Info info : aInfos)
      {
         cmbDriver.addItem(info.getName());
      }

      int driverNumber = wp.getDriver();
      if (driverNumber == -1)
      {
         driverNumber = 0;
      }

      cmbDriver.setSelectedIndex(driverNumber);

      panel.addComponentExt(cmbDriver, 190, 10, 250,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);
      cmbDriver.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            if (cmbDriver.getSelectedItem() != null)
            {

            }

         }
      });

      // Recorded File Prefix
      JLabel lblPrefix = new JLabel("Recorded file prefix:");
      panel.addComponentExt(lblPrefix, 10, 50, 180, WhistleStudio.BUTTON_HEIGHT,
            CompExtEnum.Normal);

      txtPrefix = new JTextField(wp.getPrefix());
      panel.addComponentExt(txtPrefix, 190, 50, 50, WhistleStudio.BUTTON_HEIGHT,
            CompExtEnum.Normal);

      // Recording Sample Rate
      JLabel lblSampleRate = new JLabel("Recording Sample Rate:");
      panel.addComponentExt(lblSampleRate, 10, 90, 180,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      txtSampleRate = new JTextField(String.valueOf(wp.getSampleRate()));
      panel.addComponentExt(txtSampleRate, 190, 90, 50,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      // Recording Sample Size Bits
      JLabel lblSampleBits = new JLabel("Recording Sample Size Bits:");
      panel.addComponentExt(lblSampleBits, 10, 130, 180,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      txtSampleBits = new JTextField(String.valueOf(wp.getSampleBits()));
      panel.addComponentExt(txtSampleBits, 190, 130, 50,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      // No Tone Level
      JLabel lblNoToneLevel = new JLabel("No Tone Level:");
      panel.addComponentExt(lblNoToneLevel, 10, 170, 180,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      txtNoToneLimit = new JTextField(String.valueOf(wp.getNoToneLimit()));
      panel.addComponentExt(txtNoToneLimit, 190, 170, 50,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      addSaveCancelButtons(panel, new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            WaveProperties wp = WaveProperties.getInstance();
            wp.setDriver(cmbDriver.getSelectedIndex());
            wp.setPrefix(txtPrefix.getText());
            wp.setSampleRate(Integer.parseInt(txtSampleRate.getText()));
            wp.setSampleBits(Integer.parseInt(txtSampleBits.getText()));
            wp.setNoToneLimit(Integer.parseInt(txtNoToneLimit.getText()));
            iWaveListener.processFileNoPath(iFileName);
         }
      });

      return panel;
   }

   private JComponent getFilePropertiesPanel(int width, int height)
   {
      JPanelExt panel = new JPanelExt(width, height);
      WaveProperties wp = WaveProperties.getInstance();

      // Instrument
      JLabel lblInstrument = new JLabel("Instrument:");
      panel.addComponentExt(lblInstrument, 10, 5, 120,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);
      cmbInstruments = new JComboBox();
      Soundbank soundbank = MidiCreator.getSoundBank();
      for (Instrument instrument : soundbank.getInstruments())
      {
         cmbInstruments.addItem(instrument.getName());
      }
      cmbInstruments.setSelectedIndex(0);

      panel.addComponentExt(cmbInstruments, 140, 5, 170,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.ResizeWithWidth);
      cmbInstruments.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {

         }
      });
      // Octave Change
      JLabel lblOctave = new JLabel("Octave:");
      panel.addComponentExt(lblOctave, 10, 50, 50, WhistleStudio.BUTTON_HEIGHT,
            CompExtEnum.Normal);

      // Octave Plus
      btnOctavePlus = new JButton("+");
      GraphicsUtil.adjustFontForOS(btnOctavePlus);
     btnOctavePlus.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            int value = Integer.parseInt(txtOctave.getText());
            txtOctave.setText(String.valueOf(++value));
         }

      });
      panel.addComponentExt(btnOctavePlus, 130, 50, 45,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      txtOctave = new JTextField(String.valueOf(wp.getOctave()));
      txtOctave.addFocusListener(new FocusListener()
      {
         @Override
         public void focusLost(FocusEvent e)
         {
         }

         @Override
         public void focusGained(FocusEvent e)
         {
         }
      });

      txtOctave.addKeyListener(new KeyListener()
      {
         @Override
         public void keyTyped(KeyEvent e)
         {
         }

         @Override
         public void keyReleased(KeyEvent e)
         {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
               //Set focus on something else
               btnOctavePlus.requestFocusInWindow();
            }
         }

         @Override
         public void keyPressed(KeyEvent e)
         {
         }
      });

      panel.addComponentExt(txtOctave, 180, 50, 45, WhistleStudio.BUTTON_HEIGHT,
            CompExtEnum.Normal);

      // Octave Minus
      JButton btnOctaveMInus = new JButton("-");
      GraphicsUtil.adjustFontForOS(btnOctaveMInus);
     btnOctaveMInus.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            int value = Integer.parseInt(txtOctave.getText());
            txtOctave.setText(String.valueOf(--value));
         }

      });
      panel.addComponentExt(btnOctaveMInus, 230, 50, 45,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      // Tone Height Change
      JLabel lblTone = new JLabel("Tone:");
      panel.addComponentExt(lblTone, 10, 90, 50, WhistleStudio.BUTTON_HEIGHT,
            CompExtEnum.Normal);

      // Tone Height Plus
      btnTonePlus = new JButton("+");
      GraphicsUtil.adjustFontForOS(btnTonePlus);
      btnTonePlus.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            int value = Integer.parseInt(txtTone.getText());
            txtTone.setText(String.valueOf(++value));
         }

      });
      panel.addComponentExt(btnTonePlus, 130, 90, 45,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      txtTone = new JTextField(String.valueOf(wp.getTone()));
      txtTone.addFocusListener(new FocusListener()
      {
         @Override
         public void focusLost(FocusEvent e)
         {
         }

         @Override
         public void focusGained(FocusEvent e)
         {
            // TODO Auto-generated method stub

         }
      });

      txtTone.addKeyListener(new KeyListener()
      {
         @Override
         public void keyTyped(KeyEvent e)
         {
         }

         @Override
         public void keyReleased(KeyEvent e)
         {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
               //Set focus on something else
               btnTonePlus.requestFocusInWindow();
            }
         }

         @Override
         public void keyPressed(KeyEvent e)
         {
         }
      });

      panel.addComponentExt(txtTone, 180, 90, 45, WhistleStudio.BUTTON_HEIGHT,
            CompExtEnum.Normal);

      // Tone Height Minus
      JButton btnToneMinus = new JButton("-");
      GraphicsUtil.adjustFontForOS(btnToneMinus);
      btnToneMinus.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            int value = Integer.parseInt(txtTone.getText());
            txtTone.setText(String.valueOf(--value));
         }

      });
      panel.addComponentExt(btnToneMinus, 230, 90, 45,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      // Tempo
      JLabel lblTempo = new JLabel("Tempo:");
      panel.addComponentExt(lblTempo, 10, 130, 120, WhistleStudio.BUTTON_HEIGHT,
            CompExtEnum.Normal);

      sliderTempo = new JSlider(0, 600);
      sliderTempo.addMouseMotionListener(new MouseMotionListener()
      {

         @Override
         public void mouseDragged(MouseEvent e)
         {
            sliderTempo.setToolTipText(String.valueOf(sliderTempo.getValue()));
            txtTempo.setText(String.valueOf(sliderTempo.getValue()));
         }

         @Override
         public void mouseMoved(MouseEvent e)
         {
         }
      });

      panel.addComponentExt(sliderTempo, 130, 130, width - 200,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.ResizeWithWidth);

      txtTempo = new JTextField(String.valueOf(wp.getTempo()));
      txtTempo.addFocusListener(new FocusListener()
      {
         @Override
         public void focusLost(FocusEvent e)
         {
            if (!e.isTemporary())
            {
               setSliderSafe(sliderTempo, txtTempo);
            }
         }

         @Override
         public void focusGained(FocusEvent e)
         {
         }
      });

      txtTempo.addKeyListener(new KeyListener()
      {
         @Override
         public void keyTyped(KeyEvent e)
         {
         }

         @Override
         public void keyReleased(KeyEvent e)
         {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
               setSliderSafe(sliderTempo, txtTempo);
               //Set focus on something else
               sliderTempo.requestFocusInWindow();
            }
         }

         @Override
         public void keyPressed(KeyEvent e)
         {
         }
      });

      panel.addComponentExt(txtTempo, width - 60, 130, 50,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.MoveWithWidth);

      // Byte slices
      JLabel lblBytesInSlice = new JLabel("No of bytes in slice:");
      panel.addComponentExt(lblBytesInSlice, 10, 180, 120,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      sliderBytesInSlice = new JSlider(0, 10000);
      sliderBytesInSlice.addMouseMotionListener(new MouseMotionListener()
      {

         @Override
         public void mouseDragged(MouseEvent e)
         {
            sliderBytesInSlice.setToolTipText(
                  String.valueOf(sliderBytesInSlice.getValue()));
            txtBytesInSlice
                  .setText(String.valueOf(sliderBytesInSlice.getValue()));
         }

         @Override
         public void mouseMoved(MouseEvent e)
         {
         }
      });

      panel.addComponentExt(sliderBytesInSlice, 130, 180, width - 200,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.ResizeWithWidth);

      txtBytesInSlice = new JTextField(String.valueOf(wp.getBytesInSlice()));
      txtBytesInSlice.addFocusListener(new FocusListener()
      {
         @Override
         public void focusLost(FocusEvent e)
         {
            if (!e.isTemporary())
            {
               setSliderSafe(sliderBytesInSlice, txtBytesInSlice);
            }
         }

         @Override
         public void focusGained(FocusEvent e)
         {
         }
      });

      txtBytesInSlice.addKeyListener(new KeyListener()
      {
         @Override
         public void keyTyped(KeyEvent e)
         {
         }

         @Override
         public void keyReleased(KeyEvent e)
         {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
               setSliderSafe(sliderBytesInSlice, txtBytesInSlice);
               //Set focus on something else
               sliderBytesInSlice.requestFocusInWindow();
            }
         }

         @Override
         public void keyPressed(KeyEvent e)
         {
         }
      });

      panel.addComponentExt(txtBytesInSlice, width - 60, 180, 50,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.MoveWithWidth);

      // Minimum note length
      JLabel lblNoteLength = new JLabel("Min note length:");
      panel.addComponentExt(lblNoteLength, 10, 230, 120,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      cmbNoteLength = new JComboBox();

      cmbNoteLength.addItem("No Limit");
      for (NoteLength noteLength : NoteLength.getNoteLengthList(4, 0.02))
      {
         cmbNoteLength.addItem(noteLength.getName());
      }

      cmbNoteLength.setSelectedItem(wp.getNoteLength());
      panel.addComponentExt(cmbNoteLength, 140, 230, 100,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);
      cmbNoteLength.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            if (cmbNoteLength.getSelectedItem() != null)
            {

            }

         }
      });

      // Min slice in a row
      JLabel lblMinSlice = new JLabel("Min slices in a row:");
      panel.addComponentExt(lblMinSlice, 10, 280, 120,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      sliderMinSlices = new JSlider(0, 30);
      sliderMinSlices.addMouseMotionListener(new MouseMotionListener()
      {

         @Override
         public void mouseDragged(MouseEvent e)
         {
            sliderMinSlices
                  .setToolTipText(String.valueOf(sliderMinSlices.getValue()));
            txtMinSlices.setText(String.valueOf(sliderMinSlices.getValue()));
         }

         @Override
         public void mouseMoved(MouseEvent e)
         {
         }
      });

      panel.addComponentExt(sliderMinSlices, 130, 280, width - 200,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.ResizeWithWidth);

      txtMinSlices = new JTextField(String.valueOf(wp.getMinSlices()));
      txtMinSlices.addFocusListener(new FocusListener()
      {
         @Override
         public void focusLost(FocusEvent e)
         {
            if (!e.isTemporary())
            {
               setSliderSafe(sliderMinSlices, txtMinSlices);
            }
         }

         @Override
         public void focusGained(FocusEvent e)
         {
         }
      });

      txtMinSlices.addKeyListener(new KeyListener()
      {
         @Override
         public void keyTyped(KeyEvent e)
         {
         }

         @Override
         public void keyReleased(KeyEvent e)
         {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
               setSliderSafe(sliderMinSlices, txtMinSlices);
               //Set focus on something else
               sliderMinSlices.requestFocusInWindow();
            }
         }

         @Override
         public void keyPressed(KeyEvent e)
         {
         }
      });

      panel.addComponentExt(txtMinSlices, width - 60, 280, 50,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.MoveWithWidth);

      // Min hertz filter
      JLabel lblMinHertz = new JLabel("Min hertz:");
      panel.addComponentExt(lblMinHertz, 10, 320, 120,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      sliderMinHertz = new JSlider(0, 2000);
      sliderMinHertz.addMouseMotionListener(new MouseMotionListener()
      {

         @Override
         public void mouseDragged(MouseEvent e)
         {
            sliderMinHertz
                  .setToolTipText(String.valueOf(sliderMinHertz.getValue()));
            txtMinHertz.setText(String.valueOf(sliderMinHertz.getValue()));
         }

         @Override
         public void mouseMoved(MouseEvent e)
         {
         }
      });

      panel.addComponentExt(sliderMinHertz, 130, 320, width - 200,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.ResizeWithWidth);

      txtMinHertz = new JTextField(String.valueOf(wp.getMinHertz()));
      txtMinHertz.addFocusListener(new FocusListener()
      {
         @Override
         public void focusLost(FocusEvent e)
         {
            if (!e.isTemporary())
            {
               setSliderSafe(sliderMinHertz, txtMinHertz);
            }
         }

         @Override
         public void focusGained(FocusEvent e)
         {
         }
      });

      txtMinHertz.addKeyListener(new KeyListener()
      {
         @Override
         public void keyTyped(KeyEvent e)
         {
         }

         @Override
         public void keyReleased(KeyEvent e)
         {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
               setSliderSafe(sliderMinHertz, txtMinHertz);
               //Set focus on something else
               sliderMinHertz.requestFocusInWindow();
            }
         }

         @Override
         public void keyPressed(KeyEvent e)
         {
         }
      });

      panel.addComponentExt(txtMinHertz, width - 60, 320, 50,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.MoveWithWidth);

      // Max hertz filter
      JLabel lblMaxHertz = new JLabel("Max hertz:");
      panel.addComponentExt(lblMaxHertz, 10, 360, 120,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      sliderMaxHertz = new JSlider(2000, 8000);
      sliderMaxHertz.addMouseMotionListener(new MouseMotionListener()
      {

         @Override
         public void mouseDragged(MouseEvent e)
         {
            sliderMaxHertz
                  .setToolTipText(String.valueOf(sliderMaxHertz.getValue()));
            txtMaxHertz.setText(String.valueOf(sliderMaxHertz.getValue()));
         }

         @Override
         public void mouseMoved(MouseEvent e)
         {
         }
      });

      panel.addComponentExt(sliderMaxHertz, 130, 360, width - 200,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.ResizeWithWidth);

      txtMaxHertz = new JTextField(String.valueOf(wp.getMaxHertz()));
      txtMaxHertz.addFocusListener(new FocusListener()
      {
         @Override
         public void focusLost(FocusEvent e)
         {
            if (!e.isTemporary())
            {
               setSliderSafe(sliderMaxHertz, txtMaxHertz);
            }
         }

         @Override
         public void focusGained(FocusEvent e)
         {
         }
      });

      txtMaxHertz.addKeyListener(new KeyListener()
      {
         @Override
         public void keyTyped(KeyEvent e)
         {
         }

         @Override
         public void keyReleased(KeyEvent e)
         {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
               setSliderSafe(sliderMaxHertz, txtMaxHertz);
               //Set focus on something else
               sliderMaxHertz.requestFocusInWindow();
            }
         }

         @Override
         public void keyPressed(KeyEvent e)
         {
         }
      });

      panel.addComponentExt(txtMaxHertz, width - 60, 360, 50,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.MoveWithWidth);

      // Min hertz filter
      JLabel lblToneVolLimit = new JLabel("Tone vol limit:");
      panel.addComponentExt(lblToneVolLimit, 10, 400, 120,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      sliderToneVolLimit = new JSlider(0, 300);
      sliderToneVolLimit.addMouseMotionListener(new MouseMotionListener()
      {

         @Override
         public void mouseDragged(MouseEvent e)
         {
            sliderToneVolLimit.setToolTipText(
                  String.valueOf(sliderToneVolLimit.getValue()));
            txtToneVolLimit
                  .setText(String.valueOf(sliderToneVolLimit.getValue()));
         }

         @Override
         public void mouseMoved(MouseEvent e)
         {
         }
      });

      panel.addComponentExt(sliderToneVolLimit, 130, 400, width - 200,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.ResizeWithWidth);

      txtToneVolLimit = new JTextField(String.valueOf(wp.getToneVolLimit()));
      txtToneVolLimit.addFocusListener(new FocusListener()
      {
         @Override
         public void focusLost(FocusEvent e)
         {
            if (!e.isTemporary())
            {
               setSliderSafe(sliderToneVolLimit, txtToneVolLimit);
            }
         }

         @Override
         public void focusGained(FocusEvent e)
         {
         }
      });

      txtToneVolLimit.addKeyListener(new KeyListener()
      {
         @Override
         public void keyTyped(KeyEvent e)
         {
         }

         @Override
         public void keyReleased(KeyEvent e)
         {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
               setSliderSafe(sliderToneVolLimit, txtToneVolLimit);
               //Set focus on something else
               sliderToneVolLimit.requestFocusInWindow();
            }
         }

         @Override
         public void keyPressed(KeyEvent e)
         {
         }
      });

      panel.addComponentExt(txtToneVolLimit, width - 60, 400, 50,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.MoveWithWidth);

      // Reverse checkbox
      JLabel lblReverseMaxVol = new JLabel("Reverse vol limit:");
      panel.addComponentExt(lblReverseMaxVol, 10, 440, 120,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      chkReverseMaxVol = new JCheckBox((String) null);
      panel.addComponentExt(chkReverseMaxVol, 130, 440, 20,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);

      addSaveCancelButtons(panel, new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            try
            {
               WaveProperties wp = WaveProperties.getInstance();
               wp.setInstrument((String) cmbInstruments.getSelectedItem());
               wp.setInstrumentIndex(cmbInstruments.getSelectedIndex());
               wp.setOctave(Integer.valueOf(txtOctave.getText()));
               wp.setTone(Integer.valueOf(txtTone.getText()));
               wp.setTempo(Integer.valueOf(txtTempo.getText()));
               wp.setBytesInSlice(Integer.valueOf(txtBytesInSlice.getText()));
               wp.setNoteLength((String) cmbNoteLength.getSelectedItem());
               wp.setMinSlices(Integer.valueOf(txtMinSlices.getText()));
               wp.setMinHertz(Integer.valueOf(txtMinHertz.getText()));
               wp.setMaxHertz(Integer.valueOf(txtMaxHertz.getText()));
               wp.setToneVolLimit(Integer.valueOf(txtToneVolLimit.getText()));
               wp.setVolumeLimitReverse(chkReverseMaxVol.isSelected());

               iWaveListener.processFileNoPath(iFileName);
            }
            catch (NumberFormatException e1)
            {
               // TODO Error message
               e1.printStackTrace();
            }
         }

      });

      return panel;
   }

   public static void setSliderSafe(JSlider slider, JTextField txtField)
   {
      int value = -1;
      try
      {
         value = Integer.parseInt(txtField.getText());
      }
      catch (NumberFormatException e)
      {
         txtField.setText(String.valueOf(slider.getValue()));
         return;
      }

      if (value < slider.getMinimum())
      {
         txtField.setText(String.valueOf(slider.getValue()));
         return;
      }

      if (value > slider.getMaximum())
      {
         txtField.setText(String.valueOf(slider.getValue()));
         return;
      }

      slider.setValue(value);
   }


   private void addSaveCancelButtons(JPanelExt panel,
         ActionListener saveActionListener)
   {
      // TODO Make an apply-button and close window when saving

      JButton btnAutoConfig = new JButton("AutoConfig");
      GraphicsUtil.adjustFontForOS(btnAutoConfig);
      panel.addComponentExt(btnAutoConfig, 80, 490, 120,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);
      btnAutoConfig.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            iWaveListener.autoConfig();
         }
      });

      JButton btnSave = new JButton("Save");
      GraphicsUtil.adjustFontForOS(btnSave);
      panel.addComponentExt(btnSave, 310, 490, 80, WhistleStudio.BUTTON_HEIGHT,
            CompExtEnum.Normal);
      btnSave.addActionListener(saveActionListener);

      JButton btnCancel = new JButton("Cancel");
      GraphicsUtil.adjustFontForOS(btnCancel);
      panel.addComponentExt(btnCancel, 400, 490, 80,
            WhistleStudio.BUTTON_HEIGHT, CompExtEnum.Normal);
      btnCancel.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
         }
      });

   }

   public static OptionsFrame getInstance(String fileName,
         WaveListener waveListener)
   {
      // JScorePanel jScorePanel
      if (iOptionsFrame == null)
      {
         iOptionsFrame = new OptionsFrame(fileName, waveListener);
      }

      iOptionsFrame.init(fileName);
      return iOptionsFrame;
   }

   private void init(String fileName)
   {
      WaveProperties wp = WaveProperties.getInstance();
      wp.init(fileName); // TODO Might not be necessary
      setTitle("Options for " + fileName);
      iFileName = fileName;

      // General
      cmbDriver.setSelectedIndex(wp.getDriver());
      txtPrefix.setText(wp.getPrefix());
      txtSampleRate.setText(String.valueOf(wp.getSampleRate()));
      txtSampleBits.setText(String.valueOf(wp.getSampleBits()));
      txtNoToneLimit.setText(String.valueOf(wp.getNoToneLimit()));

      // File Properties
      cmbInstruments.setSelectedItem(wp.getInstrument());
      txtOctave.setText(String.valueOf(wp.getOctave()));
      txtTone.setText(String.valueOf(wp.getTone()));
      txtTempo.setText(String.valueOf(wp.getTempo()));
      sliderTempo.setValue(wp.getTempo());
      txtBytesInSlice.setText(String.valueOf(wp.getBytesInSlice()));
      sliderBytesInSlice.setValue(wp.getBytesInSlice());
      cmbNoteLength.setSelectedItem(wp.getNoteLength());
      txtMinSlices.setText(String.valueOf(wp.getMinSlices()));
      sliderMinSlices.setValue(wp.getMinSlices());
      txtMinHertz.setText(String.valueOf(wp.getMinHertz()));
      sliderMinHertz.setValue(wp.getMinHertz());
      txtMaxHertz.setText(String.valueOf(wp.getMaxHertz()));
      sliderMaxHertz.setValue(wp.getMaxHertz());
      txtToneVolLimit.setText(String.valueOf(wp.getToneVolLimit()));
      sliderToneVolLimit.setValue(wp.getToneVolLimit());
      chkReverseMaxVol.setSelected(wp.isVolumeLimitReverse());
   }

}
