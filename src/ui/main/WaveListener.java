package ui.main;

import core.tones.JScorePanel;

public interface WaveListener
{

   JScorePanel getScorePanel();

   void processFileNoPath(String fileName);

   void initOptions(String wavFilename);

   void autoConfig();

}
