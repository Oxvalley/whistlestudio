package ui.main;

import java.util.List;

import javax.swing.JOptionPane;

import common.properties.SettingProperties;

public class WaveProperties
{

   private static WaveProperties iWaveProp;
   private SettingProperties iProp;
   private String iPropKey;
   private String iFileName;
   private String iOldFileName;
   private boolean iIsTempDummy;

   private WaveProperties(boolean isTempDummy)
   {
      iIsTempDummy = isTempDummy;
      iProp = SettingProperties.getInstance();
   }

   public static WaveProperties getInstance()
   {
      if (iWaveProp == null)
      {
         iWaveProp = new WaveProperties(false);
      }
      return iWaveProp;
   }

   public String getFileName()
   {
      return iFileName;
   }

   public boolean init(String fileName)
   {
      iOldFileName = iFileName;
      iFileName = fileName;
      if (!iIsTempDummy)
      {
         iProp.setProperty(WhistlePropertyConstants.WAVE_LAST_USED, fileName);
         System.out.println("Last used: " + fileName);
      }

      iPropKey = getWaveId(fileName);

      if (iPropKey == null)
      {
         // New WaveFile
         String id = iProp.getInnerNextNumber("wave.filename.id", "001", false);
         iPropKey = "wave.id" + id;
         iProp.setProperty("wave.filename.id" + id, fileName);

         if (iOldFileName != null)
         {
            // Copy all settings from last filename
            WaveProperties oldwP = new WaveProperties(true);
            oldwP.init(iOldFileName);

            // General Properties
            setDriver(oldwP.getDriver());
            setPrefix(oldwP.getPrefix());
            setSampleRate(oldwP.getSampleRate());
            setSampleBits(oldwP.getSampleBits());
            setNoToneLimit(oldwP.getNoToneLimit());

            // File properties
            setInstrument(oldwP.getInstrument());
            setInstrumentIndex(oldwP.getInstrumentIndex());
            setOctave(oldwP.getOctave());
            setTone(oldwP.getTone());
            setTempo(oldwP.getTempo());
            setBytesInSlice(oldwP.getBytesInSlice());
            setNoteLength(oldwP.getNoteLength());
            setMinSlices(oldwP.getMinSlices());
            setMinHertz(oldwP.getMinHertz());
            setMaxHertz(oldwP.getMaxHertz());
            setToneVolLimit(oldwP.getToneVolLimit());
            setVolumeLimitReverse(oldwP.isVolumeLimitReverse());
         }

         iProp.save();

         int dialogResult = JOptionPane.showConfirmDialog(null,
               "This is a new file! Do you want to run AutoConfig?", "Question",
               JOptionPane.YES_NO_CANCEL_OPTION);
         if (dialogResult == JOptionPane.YES_OPTION)
         {
            return true;
         }

      }
      else
      {
         iPropKey = "wave." + iPropKey;
      }

      return false;
   }

   public void setInstrumentIndex(int index)
   {
      iProp.setProperty(iPropKey + WhistlePropertyConstants.INSTRUMENT_INDEX,
            index);
   }

   public String getWaveId(String fileName)
   {
      String propKey = null;
      List<String> lst = iProp.getPropertyKeyList("wave.filename.id");
      for (String key : lst)
      {
         if (iProp.getProperty(key).equals(fileName))
         {
            propKey = key.substring(key.lastIndexOf(".id") + 1);
         }
      }
      return propKey;
   }

   public void setInstrument(String instrument)
   {
      iProp.setProperty(iPropKey + WhistlePropertyConstants.INSTRUMENT,
            instrument);
   }

   public void setOctave(int octave)
   {
      iProp.setProperty(iPropKey + WhistlePropertyConstants.OCTAVE, octave);
   }

   public void setTone(int tone)
   {
      iProp.setProperty(iPropKey + WhistlePropertyConstants.TONE, tone);
   }

   public void setTempo(int tempo)
   {
      iProp.setProperty(iPropKey + WhistlePropertyConstants.TEMPO, tempo);
   }

   public void setBytesInSlice(int bytesInSlice)
   {
      iProp.setProperty(iPropKey + WhistlePropertyConstants.BYTES_IN_SLICE,
            bytesInSlice);
   }

   public void setNoteLength(String noteLength)
   {
      iProp.setProperty(iPropKey + WhistlePropertyConstants.NOTE_LENGTH,
            noteLength);
   }

   public void setMinSlices(int minSlices)
   {
      iProp.setProperty(iPropKey + WhistlePropertyConstants.MIN_SLICE_IN_A_ROW,
            minSlices);
   }

   public void setMinHertz(int minHertz)
   {
      iProp.setProperty(iPropKey + WhistlePropertyConstants.MIN_HERTZ,
            minHertz);
   }

   public void setMaxHertz(int maxHertz)
   {
      iProp.setProperty(iPropKey + WhistlePropertyConstants.MAX_HERTZ,
            maxHertz);
   }

   public void setToneVolLimit(Integer volumeLimit)
   {
      iProp.setProperty(iPropKey + WhistlePropertyConstants.TONE_VOL_LIMIT,
            volumeLimit);
   }

   public void setVolumeLimitReverse(boolean volumeLimitReverse)
   {
      iProp.setProperty(
            iPropKey + WhistlePropertyConstants.LIMIT_VOLUME_REVERSE,
            volumeLimitReverse);
   }

   // General Properties

   public int getDriver()
   {
      return iProp.getIntProperty(
            WhistlePropertyConstants.RECORDING_DRIVER_NUMBER, 0);
   }

   public String getPrefix()
   {
      return iProp.getProperty(WhistlePropertyConstants.WAVE_FILE_PREFIX,
            "rec");
   }

   public int getSampleRate()
   {
      return iProp.getIntProperty(
            WhistlePropertyConstants.RECORDING_SAMPLE_RATE, 22050);
   }

   public int getSampleBits()
   {
      return iProp.getIntProperty(
            WhistlePropertyConstants.RECORDING_SAMPLE_SIZE_BITS, 8);
   }

   public int getNoToneLimit()
   {
      return iProp.getIntProperty(WhistlePropertyConstants.NO_TONE_LIMIT, 8);
   }

   public void setDriver(int selectedIndex)
   {
      iProp.setProperty(WhistlePropertyConstants.RECORDING_DRIVER_NUMBER,
            selectedIndex);
   }

   public void setPrefix(String prefix)
   {
      iProp.setProperty(WhistlePropertyConstants.WAVE_FILE_PREFIX, prefix);
   }

   public void setSampleRate(int sampleRate)
   {
      iProp.setProperty(WhistlePropertyConstants.RECORDING_SAMPLE_RATE,
            sampleRate);
   }

   public void setSampleBits(int sampleBits)
   {
      iProp.setProperty(WhistlePropertyConstants.RECORDING_SAMPLE_SIZE_BITS,
            sampleBits);
   }

   public void setNoToneLimit(int limit)
   {
      iProp.setProperty(WhistlePropertyConstants.NO_TONE_LIMIT, limit);
   }

   public String getInstrument()
   {
      return iProp.getProperty(iPropKey + WhistlePropertyConstants.INSTRUMENT,
            "");
   }

   public int getOctave()
   {
      return iProp.getIntProperty(iPropKey + WhistlePropertyConstants.OCTAVE,
            0);
   }

   public int getTone()
   {
      return iProp.getIntProperty(iPropKey + WhistlePropertyConstants.TONE, 0);
   }

   public int getTempo()
   {
      return iProp.getIntProperty(iPropKey + WhistlePropertyConstants.TEMPO,
            WhistlePropertyConstants.DEFAULT_TEMPO);
   }

   public int getBytesInSlice()
   {
      return iProp.getIntProperty(
            iPropKey + WhistlePropertyConstants.BYTES_IN_SLICE, 512);
   }

   public String getNoteLength()
   {
      return iProp.getProperty(iPropKey + WhistlePropertyConstants.NOTE_LENGTH,
            "1/32");
   }

   public int getMinSlices()
   {
      return iProp.getIntProperty(
            iPropKey + WhistlePropertyConstants.MIN_SLICE_IN_A_ROW, 2);
   }

   public int getMinHertz()
   {
      return iProp.getIntProperty(iPropKey + WhistlePropertyConstants.MIN_HERTZ,
            500);
   }

   public int getMaxHertz()
   {
      return iProp.getIntProperty(iPropKey + WhistlePropertyConstants.MAX_HERTZ,
            5000);
   }

   public int getToneVolLimit()
   {
      return iProp.getIntProperty(
            iPropKey + WhistlePropertyConstants.TONE_VOL_LIMIT, 88);
   }

   public boolean isVolumeLimitReverse()
   {
      return iProp.getBooleanProperty(
            iPropKey + WhistlePropertyConstants.LIMIT_VOLUME_REVERSE, false);
   }

   public int getInstrumentIndex()
   {
      return iProp.getIntProperty(
            iPropKey + WhistlePropertyConstants.INSTRUMENT_INDEX, 0);
   }

}
