package ui.main;

public class WhistlePropertyConstants
{

   public static final String WAVE_LAST_USED = "wave.last.used";
   public static final String APP_VERSION = "app.version";
   public static final String APP_NAME = "app.name";
   public static final String IMPORT_WAVE_PATH = "import.wave.path";
   public static final String WAVE_FILE_PREFIX = "wave.file.prefix";
   public static final String RECORDING_SAMPLE_RATE = "recording.sample.rate";
   public static final String RECORDING_SAMPLE_SIZE_BITS =
         "recording.sample.bits";
   public static final String NO_TONE_LIMIT = "no.tone.limit";
   public static final String RECORDING_DRIVER_NUMBER =
         "recording.driver.number";
   public static final String LIMIT_VOLUME_REVERSE = ".volume.reverse";
   public static final String VOLUME_MAX_FACTOR = ".max.factor";
   public static final String MAX_HERTZ = ".max.hertz";
   public static final String MIN_HERTZ = ".min.hertz";
   public static final String MIN_SLICE_IN_A_ROW = ".slice.in.a.row";
   public static final String OCTAVE = ".octave";
   public static final String TONE = ".tone";
   public static final String TEMPO = ".tempo";
   public static final String BYTES_IN_SLICE = ".bytes.slice";
   public static final String NOTE_LENGTH = ".note.length";
   public static final String INSTRUMENT = ".instrument";
   public static final String TONE_VOL_LIMIT = ".tone.volume.limit";
   public static final String INSTRUMENT_INDEX = ".instrument.index";
   public static final int DEFAULT_TEMPO = 128;
   public static final String JMIDISHEETMUSIC_JAR = "jmidisheetmusic.jar";

   public static final String J_MIDI_SHEET_MUSIC_FOLDER_PREFIX =
         "JMidiSheetMusic_v";
   public static final String J_MIDI_SHEET_MUSIC_JAR_PREFIX = "JMusic_v";

}
