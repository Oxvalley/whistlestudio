package ui.main;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeriesCollection;

import common.properties.SettingProperties;
import common.ui.components.ext.CompExtEnum;
import common.ui.components.ext.JPanelExt;
import common.ui.components.ext.JScrollPaneExt;
import common.utils.AppInfoInterface;
import common.utils.FileHelper;
import common.utils.GraphicsUtil;
import common.utils.RunDosUtil;
import core.converters.Midi2WavRender;
import core.converters.Mp3Creator;
import core.midi.MidiCreator;
import core.midi.MidiPlayer;
import core.midi.WaitForSong;
import core.tones.FrequenceFinder;
import core.tones.JScorePanel;
import core.tones.JTonePanel;
import core.tones.NoteLength;
import core.tones.SampleDiagram;
import core.tones.TagFilter;
import core.tones.ToneData;
import core.wav.WaveData;
import core.wav.WavePlayer;
import core.wav.record.BufferContainer;
import core.wav.record.CaptureThread;
import diagrams.Charter;
import system.AppInfo;

public class WhistleStudio extends JFrame implements MouseMotionListener,
      ActionListener, MouseListener, WaveListener
{
   /**
    *
    */
   private static final long serialVersionUID = -3972296123492988634L;

   private enum buttonText
   {
      Record, Player, MidiPlayer, Export, Analyze, Options, AutoConfig, About
   }

   public static final int BUTTON_HEIGHT = 30;

   private static SettingProperties iProp;
   public static final String MIDI_FOLDER = "midi";
   public static final String MP3_FOLDER = "mp3";
   public static final String WAVE_FOLDER = "waves";
   private static final int NA = 0;

   public static final String TAG_MID = "mid";
   public static final String TAG_WAV = "wav";
   public static final String TAG_MP3 = "mp3";

   public static final String NEW_WAVE_FOLDER = "waves/new";
   protected static final int PLAY = 2;
   private static final int RECORD = 1;
   private static final int RIGHT_MENU_WIDTH = 130;
   public static final String TEMP_MIDI_FILE = MIDI_FOLDER + "/temp.mid";
   protected static final String TEMP_WAV_FILE = NEW_WAVE_FOLDER + "/temp.wav";
   private static double toneVolumeMax = -1;
   private static double toneVolumeMaxFactor;

   public static final String WAVE_RECORDING_FOLDER = "waves/recordings";
   public static final String ICON_NAME = "whistle.png";

   private static final FileManager iFileManager =
         new FileManager((new File(WAVE_FOLDER)).getAbsolutePath(), "wav");

   private boolean iInitDone = false;

   public static void enableButtons(List<JButton> list, boolean value)
   {
      for (JButton button : list)
      {
         button.setEnabled(value);
      }
   }

   public void setInitDone(Boolean value)
   {
      iInitDone = value;
   }

   public static String getNextFreeFileName(String fileName)
   {
      File file = new File(fileName);
      if (!file.exists())
      {
         return fileName;
      }

      String tag = "";
      String fileNoTag;

      // Remove tag
      int i = fileName.lastIndexOf(".");

      if (i == -1)
      {
         // No dot in filename
         fileNoTag = fileName;
      }
      else
      {
         tag = fileName.substring(i);
         fileNoTag = fileName.substring(0, i);
      }

      // Does it end with a number??
      int j;
      for (j = fileNoTag.length() - 1; j > 0; j--)
      {
         char c = fileNoTag.charAt(j);
         if (!Character.isDigit(c))
         {
            break;
         }
      }

      String fileNoNumber = fileNoTag.substring(0, j + 1);
      String number;
      if (j < fileNoTag.length() - 1)
      {
         number = fileNoTag.substring(j + 1);
      }
      else
      {
         number = "0";
      }

      // What number?
      int num = Integer.valueOf(number).intValue();

      // Increase by one
      num++;

      // put it all together
      String newFileName = fileNoNumber + num + tag;

      // Is that name free?
      return getNextFreeFileName(newFileName);

   }

   public static SettingProperties getProp()
   {
      return iProp;
   }

   public static void main(String[] args)
   {
      DebugModeEnum debugMode = DebugModeEnum.Unknown;
      if (args.length == 0 || args[0].equalsIgnoreCase("Zero"))
      {
         debugMode = DebugModeEnum.Zero;
      }
      else
      {
         if (args[0].equalsIgnoreCase("One"))
         {
            debugMode = DebugModeEnum.One;
         }
      }

      WhistleStudio whistle = WhistleStudioFactory.getWhistleStudioInstance();

      whistle.setDebugMode(debugMode);
   }

   public void setDebugMode(DebugModeEnum mode)
   {
      iDebugMode = mode;
   }

   private String appName;

   private JButton btnMidi = new JButton();
   private JButton btnImportWave = new JButton();
   private JButton btnNewWave = new JButton();
   private JButton btnPlayWave = new JButton();
   private JButton btnRecord = new JButton();
   private JButton btnStopMidi = new JButton();
   private JButton btnStopNewWave = new JButton();
   private JButton btnStopRecording = new JButton();
   private JButton btnStopWave = new JButton();
   private CaptureThread captureThread;
   private ChartPanel chartPanel = null;
   private JComboBox iCmbFilenames;
   private Color iFilesColor;
   private List<JTonePanel> iJToneList = new ArrayList<JTonePanel>();
   private List<JPanelExt> iPages = new ArrayList<JPanelExt>();
   private JPanelExt iPanelExt;
   private JPanelExt iPanelMain;
   private JPanelExt iPanelUpper;
   private int iRecordMode = NA;
   private SampleDiagram iSampleDiagram;
   private double iSampleTime;
   private List<ToneData> iToneDataList;
   private XYSeriesCollection mDataset;
   private MidiPlayer midiPlayer;
   private final List<JButton> playList = new ArrayList<JButton>();
   private String recordedfileName;

   private JScrollPaneExt scr;

   private final List<JButton> stopList = new ArrayList<JButton>();
   private WavePlayer wavePlayer;
   private DebugModeEnum iDebugMode = DebugModeEnum.Zero;
   private JScorePanel jScorePanel;

   private JTextField txtSaveMP3Path;

   private JTextField txtSaveMidiPath;

   private JTextField txtSaveWavePath;
   private static WhistleStudio iWhistleStudio;

   private WhistleStudio()
   {
      int width = 800;
      int height = 600;
      setSize(width, height);
      setIconImage(getToolkit().getImage(ICON_NAME));
      addMouseMotionListener(this);

      AppInfoInterface appinfo = AppInfo.getInstance();

      iProp = SettingProperties.getInstance();
      appName = iProp.getProperty(WhistlePropertyConstants.APP_NAME,
            appinfo.getApplicationName()) + " version "
            + iProp.getProperty(WhistlePropertyConstants.APP_VERSION,
                  appinfo.getVersion());

      setTitle(appName);
      iPanelExt = new JPanelExt(width, height);

      getContentPane().add(iPanelExt);

      double upperHeightFactor = 0.13;

      iPanelUpper =
            new JPanelExt(width, (int) Math.round(height * upperHeightFactor));
      iPanelExt.addComponentExt(iPanelUpper, 0, 0, width,
            (int) Math.round(height * upperHeightFactor),
            CompExtEnum.ResizeWithWidth);

      int x = 10;
      int buttonWidth;
      for (buttonText text : buttonText.values())
      {
         if (text != buttonText.About)
         {
            JButton btn = new JButton(text.toString());
            GraphicsUtil.adjustFontForOS(btn);
            buttonWidth = (int) Math.round(12.5 * text.toString().length());
            iPanelUpper.addComponentExt(btn, x, 10, buttonWidth, BUTTON_HEIGHT);
            btn.addActionListener(this);
            x += 10 + buttonWidth;
         }

      }

      JLabel icon = new JLabel(new ImageIcon(ICON_NAME));
      String about = "About " + appName;
      icon.setToolTipText(about);
      icon.addMouseListener(this);
      iPanelUpper.addComponentExt(icon, 690, 45, BUTTON_HEIGHT, BUTTON_HEIGHT,
            CompExtEnum.Normal);

      initChart();

      btnImportWave.setText("Import wave");
      GraphicsUtil.adjustFontForOS(btnImportWave);
      btnImportWave.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            // Create a file chooser
            final JFileChooser dialog = new JFileChooser();
            SettingProperties prop = SettingProperties.getInstance();
            String importPath =
                  prop.getProperty(WhistlePropertyConstants.IMPORT_WAVE_PATH);
            if (importPath != null)
            {
               dialog.setCurrentDirectory(new File(importPath));
            }

            // Add a custom file filter
            dialog.addChoosableFileFilter(new TagFilter("wav"));

            // Show it.
            int returnVal = dialog.showDialog(null, "Select");

            // Process the results.
            if (returnVal == JFileChooser.APPROVE_OPTION)
            {
               File fromFile = dialog.getSelectedFile();
               try
               {
                  FileHelper.copy(fromFile, new File(
                        WAVE_FOLDER + File.separator + fromFile.getName()),
                        true);
               }
               catch (IOException e1)
               {
                  e1.printStackTrace();
               }
               refreshFileCombo(fromFile.getName());
               prop.setProperty(WhistlePropertyConstants.IMPORT_WAVE_PATH,
                     fromFile.getParent());
            }
         }

      });
      iPanelUpper.addComponentExt(btnImportWave, 180, 45, 125, BUTTON_HEIGHT,
            CompExtEnum.Normal);

      iCmbFilenames = new JComboBox();
      String lastFilename =
            iProp.getProperty(WhistlePropertyConstants.WAVE_LAST_USED, "");
      refreshFileCombo(lastFilename);
      String currentFile = (String) iCmbFilenames.getSelectedItem();

      WaveProperties wp = WaveProperties.getInstance();
      boolean runAutoConfig = wp.init(currentFile);

      iPanelUpper.addComponentExt(iCmbFilenames, 315, 45, 172, BUTTON_HEIGHT,
            CompExtEnum.Normal);
      iCmbFilenames.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            if (iInitDone)
            {
               String wavfileName = (String) iCmbFilenames.getSelectedItem();

               if (wavfileName != null)
               {
                  WaveProperties wp = WaveProperties.getInstance();
                  boolean runAutoConfig = wp.init(wavfileName);
                  processFileNoPath(wavfileName);
                  initOptions(wavfileName);
                  updateExportPaths(wavfileName);
                  if (runAutoConfig)
                  {
                     showPage(buttonText.AutoConfig);
                  }
               }

            }

         }

      });

      initPlayStopButtons();

      // Play Midi Button
      btnMidi.setText("Play Midi");
      GraphicsUtil.adjustFontForOS(btnMidi);
      btnMidi.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            WaveProperties wp = WaveProperties.getInstance();
            showPage(buttonText.Player);
            playMidi(wp.getInstrumentIndex(), btnStopMidi);
         }

      });
      iPanelUpper.addComponentExt(btnMidi, 10, 45, 90, BUTTON_HEIGHT,
            CompExtEnum.Normal);

      btnStopMidi.setText("Stop");
      GraphicsUtil.adjustFontForOS(btnStopMidi);
      btnStopMidi.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            stopPlayingMidi(btnStopMidi);
         }

      });
      iPanelUpper.addComponentExt(btnStopMidi, 110, 45, 60, BUTTON_HEIGHT,
            CompExtEnum.Normal);

      btnPlayWave.setText("Play wave");
      GraphicsUtil.adjustFontForOS(btnPlayWave);
      btnPlayWave.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            enableButtons(playList, false);
            btnStopWave.setEnabled(true);
            wavePlayer = new WavePlayer(iFileManager
                  .getFullPath((String) iCmbFilenames.getSelectedItem()));
            wavePlayer.start();

            while (wavePlayer.getClip() == null)
            {
            }

            System.out.println("Length in seconds:"
                  + wavePlayer.getMicrosecondLength() / 1000);

            WaitForSong wait =
                  new WaitForSong(wavePlayer.getClip(), playList, stopList);
            wait.start();
         }

      });
      iPanelUpper.addComponentExt(btnPlayWave, 498, 45, 95, BUTTON_HEIGHT,
            CompExtEnum.Normal);

      btnStopWave.setText("Stop");
      GraphicsUtil.adjustFontForOS(btnStopWave);
      btnStopWave.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            enableButtons(playList, true);
            btnStopWave.setEnabled(false);
            wavePlayer.stopPlaying();
         }
      });
      iPanelUpper.addComponentExt(btnStopWave, 603, 45, 70, BUTTON_HEIGHT,
            CompExtEnum.Normal);

      iPanelMain = new JPanelExt(width,
            (int) Math.round(height * (1 - upperHeightFactor)));
      iPanelExt.addComponentExt(iPanelMain, 0,
            (int) Math.round(height * upperHeightFactor), width,
            (int) Math.round(height * (1 - upperHeightFactor)),
            CompExtEnum.ResizeWithWidthAndHeight);

      createMenuPages();

      processFileNoPath(currentFile);

      showPage(buttonText.Analyze);

      setDefaultCloseOperation(EXIT_ON_CLOSE);
      addWindowListener(new WindowAdapter()
      {
         @Override
         public void windowClosing(WindowEvent we)
         {
            iProp.save();
         }
      });

      setVisible(true);

      if (runAutoConfig)
      {
         javax.swing.SwingUtilities.invokeLater(new Runnable()
         {
            @Override
            public void run()
            {
               showPage(buttonText.AutoConfig);
            }
         });
      }

      iInitDone = true;
   }

   @Override
   public void processFileNoPath(String currentFile)
   {
      processFile(iFileManager.getFullPath(currentFile));
   }

   @Override
   public void initOptions(String wavfileName)
   {
      OptionsFrame.getInstance(wavfileName, this);
   }

   @Override
   public final void actionPerformed(ActionEvent arg)
   {
      if (arg.getSource() instanceof BufferContainer)
      {
         BufferContainer bufferContainer = (BufferContainer) arg.getSource();
         if (bufferContainer.getBuffer() == null)
         {
            enableButtons(playList, true);
            enableButtons(stopList, false);
         }

         iSampleDiagram.addBuffer(bufferContainer);
         iSampleTime++;
         iSampleDiagram.setTime(iSampleTime);
         iSampleDiagram.repaint();
         return;
      }

      if (arg.getSource() instanceof JButton)
      {
         JButton btn = (JButton) arg.getSource();
         buttonText textIndex = buttonText.valueOf(btn.getText());
         showPage(textIndex);
      }
   }

   public final void browseForPath(final JTextField txtPath, String tag)
   {
      File file2 = saveFile(txtPath.getText(), new TagFilter(tag));
      if (file2 != null)
      {
         String path = file2.getAbsolutePath();
         if (!path.endsWith("." + tag))
         {
            path += "." + tag;
         }

         txtPath.setText(path);
      }
   }

   private void createAboutPage(JPanelExt panel)
   {
      String html = "<html>\n";
      html += "<body bgcolor=\"#FFFF00\">\n";
      html += "<p><h1 align=\"center\">About " + appName + "</h1>\n";
      html += "<h2 align=\"center\">Created by: Lars Svensson</h2>\n";
      html += "<h3 align=\"center\"/>\n";
      html += "<h3 align=\"center\"></h3>\n";
      html +=
            "<p align=\"center\"><a href=\"mailto:lars.svensson@oxvalley.com\">"
                  + "lars.svensson@oxvalley.com</a></p>\n";
      html +=
            "<p align=\"center\"><a href=\"http://www.oxvalley.com\">www.oxvalley.com</a></p>\n";
      html += "<p align=\"center\">(C) Oxvalley Software "
            + Calendar.getInstance().get(Calendar.YEAR) + " </p>\n";
      html += "</body>";
      html += "</html>";

      JEditorPane lblAbout = new JEditorPane();
      lblAbout.setContentType("text/html");
      lblAbout.setEditable(false);
      lblAbout.setText(html);
      panel.addComponentExt(lblAbout, 10, 10, iPanelMain.getCompExtWidth() - 10,
            iPanelMain.getCompExtHeight(),
            CompExtEnum.ResizeWithWidthAndHeight);
   }

   private void createAnalyzePage(JPanelExt panel)
   {
      panel.addComponentExt(chartPanel, 10, 10,
            iPanelMain.getCompExtWidth() - 20,
            iPanelMain.getCompExtHeight() - 20,
            CompExtEnum.ResizeWithWidthAndHeight);
   }

   private void createAutoConfigPage(JPanelExt panel)
   {
      JLabel lblAnalyzing = new JLabel("Analyzing...");
      panel.addComponentExt(lblAnalyzing, 10, 40, 100, BUTTON_HEIGHT,
            CompExtEnum.Normal);

      JFreeChart chart = Charter.createChart(mDataset, "Auto config");
      ChartPanel chartPanelAuto = new ChartPanel(chart);
      panel.addComponentExt(chartPanelAuto, 10, 40,
            iPanelMain.getCompExtWidth() - 20,
            iPanelMain.getCompExtHeight() - 220,
            CompExtEnum.ResizeWithWidthAndHeight);
   }

   private void createExportPage(JPanelExt panel)
   {
      int height = 40 + (0 * (BUTTON_HEIGHT + 10));
      // Save as Midi Path Label
      JLabel lblSaveMidiPath = new JLabel("Path:");
      panel.addComponentExt(lblSaveMidiPath, 10, height, 30, BUTTON_HEIGHT,
            CompExtEnum.Normal);

      // Save as Midi Path Textfield
      txtSaveMidiPath = new JTextField();

      panel.addComponentExt(txtSaveMidiPath, 50, height,
            iPanelMain.getCompExtWidth() - RIGHT_MENU_WIDTH - 150,
            BUTTON_HEIGHT, CompExtEnum.ResizeWithWidth);

      JButton btnBrowseMidi = new JButton("Browse");
      GraphicsUtil.adjustFontForOS(btnBrowseMidi);
      btnBrowseMidi.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            browseForPath(txtSaveMidiPath, TAG_MID);
         }

      });
      panel.addComponentExt(btnBrowseMidi,
            iPanelMain.getCompExtWidth() - RIGHT_MENU_WIDTH - 90, height, 80,
            BUTTON_HEIGHT, CompExtEnum.MoveWithWidth);

      JButton btnSaveMidi = new JButton("Save as Midi");
      GraphicsUtil.adjustFontForOS(btnSaveMidi);
      btnSaveMidi.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            WaveProperties wp = WaveProperties.getInstance();
            MidiCreator wc =
                  new MidiCreator(txtSaveMidiPath.getText(), iToneDataList);
            wc.createMidiFile(wp.getInstrumentIndex(), false);
         }
      });

      panel.addComponentExt(btnSaveMidi,
            iPanelMain.getCompExtWidth() - RIGHT_MENU_WIDTH, height,
            RIGHT_MENU_WIDTH - 10, BUTTON_HEIGHT, CompExtEnum.MoveWithWidth);

      // Save as Wave Path Label
      height = 40 + (1 * (BUTTON_HEIGHT + 10));
      JLabel lblSaveWavePath = new JLabel("Path:");
      panel.addComponentExt(lblSaveWavePath, 10, height, 30, BUTTON_HEIGHT,
            CompExtEnum.Normal);

      // Save as Wav Path Textfield
      txtSaveWavePath = new JTextField();
      panel.addComponentExt(txtSaveWavePath, 50, height,
            iPanelMain.getCompExtWidth() - RIGHT_MENU_WIDTH - 150,
            BUTTON_HEIGHT, CompExtEnum.ResizeWithWidth);

      JButton btnBrowseWave = new JButton("Browse");
      GraphicsUtil.adjustFontForOS(btnBrowseWave);
      btnBrowseWave.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            browseForPath(txtSaveWavePath, TAG_WAV);
         }

      });
      panel.addComponentExt(btnBrowseWave,
            iPanelMain.getCompExtWidth() - RIGHT_MENU_WIDTH - 90, height, 80,
            BUTTON_HEIGHT, CompExtEnum.MoveWithWidth);

      JButton btnSaveWave = new JButton("Save as Wav");
      GraphicsUtil.adjustFontForOS(btnSaveWave);
      btnSaveWave.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {

            WaveProperties wp = WaveProperties.getInstance();
            MidiCreator wc = new MidiCreator(TEMP_MIDI_FILE, iToneDataList);
            wc.createMidiFile(wp.getInstrumentIndex(), false);

            Midi2WavRender.getWavFromMidi(TEMP_MIDI_FILE,
                  txtSaveWavePath.getText(), wp.getTempo());
         }
      });

      panel.addComponentExt(btnSaveWave,
            iPanelMain.getCompExtWidth() - RIGHT_MENU_WIDTH, height,
            RIGHT_MENU_WIDTH - 10, BUTTON_HEIGHT, CompExtEnum.MoveWithWidth);

      // Save as MP3 Path Label
      height = 40 + (2 * (BUTTON_HEIGHT + 10));
      JLabel lblSaveMP3Path = new JLabel("Path:");
      panel.addComponentExt(lblSaveMP3Path, 10, height, 30, BUTTON_HEIGHT,
            CompExtEnum.Normal);

      // Save as MP3 Path Textfield
      txtSaveMP3Path = new JTextField();
      panel.addComponentExt(txtSaveMP3Path, 50, height,
            iPanelMain.getCompExtWidth() - RIGHT_MENU_WIDTH - 150,
            BUTTON_HEIGHT, CompExtEnum.ResizeWithWidth);

      JButton btnBrowseMP3 = new JButton("Browse");
      btnBrowseMP3.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            browseForPath(txtSaveMP3Path, TAG_MP3);
         }

      });
      panel.addComponentExt(btnBrowseMP3,
            iPanelMain.getCompExtWidth() - RIGHT_MENU_WIDTH - 90, height, 80,
            BUTTON_HEIGHT, CompExtEnum.MoveWithWidth);

      JButton btnSaveMP3 = new JButton("Save as MP3");
      GraphicsUtil.adjustFontForOS(btnSaveMP3);
      btnSaveMP3.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            WaveProperties wp = WaveProperties.getInstance();
            MidiCreator wc = new MidiCreator(TEMP_MIDI_FILE, iToneDataList);
            wc.createMidiFile(wp.getInstrumentIndex(), false);

            Midi2WavRender.getWavFromMidi(TEMP_MIDI_FILE, TEMP_WAV_FILE,
                  wp.getTempo());

            Mp3Creator.convertToMp3(TEMP_WAV_FILE, txtSaveMP3Path.getText());
         }
      });

      panel.addComponentExt(btnSaveMP3,
            iPanelMain.getCompExtWidth() - RIGHT_MENU_WIDTH, height,
            RIGHT_MENU_WIDTH - 10, BUTTON_HEIGHT, CompExtEnum.MoveWithWidth);

      updateExportPaths(iCmbFilenames.getSelectedItem());
   }

   private void updateExportPaths(Object selectedFile)
   {
      setFilePath(txtSaveMidiPath, MIDI_FOLDER, selectedFile, TAG_MID);
      setFilePath(txtSaveMP3Path, MP3_FOLDER, selectedFile, TAG_MP3);
      setFilePath(txtSaveWavePath, NEW_WAVE_FOLDER, selectedFile, TAG_WAV);
   }

   private static void setFilePath(JTextField txt, String path,
         Object selectedItem, String tag)
   {
      String fileName = (String) selectedItem;
      fileName = FileHelper.removeTag(fileName);
      txt.setText(new File(path + File.separator + fileName + "." + tag)
            .getAbsolutePath());
   }

   private void createMenuPages()
   {

      for (buttonText text : buttonText.values())
      {
         JPanelExt panel = new JPanelExt(iPanelMain.getCompExtWidth(),
               iPanelMain.getCompExtHeight());
         iPanelMain.addComponentExt(panel, 0, 0, iPanelMain.getCompExtWidth(),
               iPanelMain.getCompExtHeight(),
               CompExtEnum.ResizeWithWidthAndHeight);

         switch (text)
         {
            case Record:
               createRecordPage(panel);
               break;

            case Player:
               createPlayerPage(panel);
               break;

            case MidiPlayer:
               // playMidiExternal();
               break;
            case Export:
               createExportPage(panel);
               break;

            case Analyze:
               createAnalyzePage(panel);
               break;

            case Options:
               break;

            case AutoConfig:
               createAutoConfigPage(panel);
               break;

            case About:
               createAboutPage(panel);
               break;
         }
         iPages.add(panel);
      }
   }

   private void createPlayerPage(final JPanelExt panel)
   {
      // JTones panel
      scr = new JScrollPaneExt(panel.getCompExtWidth() - RIGHT_MENU_WIDTH,
            panel.getCompExtHeight());
      final JTonePanel jTonePanel =
            new JTonePanel(panel.getCompExtWidth() - RIGHT_MENU_WIDTH,
                  panel.getCompExtHeight(), scr);
      iJToneList.add(jTonePanel);
      scr.addViewPortExt(jTonePanel);
      scr.setHorizontalScrollBarPolicy(
            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
      scr.setVerticalScrollBarPolicy(
            ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
      panel.addComponentExt(scr, 0, 0, panel.getCompExtWidth() - 5,
            panel.getCompExtHeight() - 5, CompExtEnum.ResizeWithWidthAndHeight);

      iCmbFilenames.addItemListener(new ItemListener()
      {

         @Override
         public void itemStateChanged(ItemEvent e)
         {
            iCmbFilenames.setBackground(iFilesColor);
         }
      });

      enableButtons(playList, true);
      enableButtons(stopList, false);
   }

   private void playMidiExternal()
   {

      WaveProperties wp = WaveProperties.getInstance();
      String fileName = (String) iCmbFilenames.getSelectedItem();

      if (fileName.toLowerCase().endsWith(".wav"))
      {
         fileName = fileName.substring(0, fileName.length() - 4) + ".mid";
      }
      else
      {
         fileName += ".mid";
      }
      String filePath =
            new File(MIDI_FOLDER).getAbsolutePath() + File.separator + fileName;

      MidiCreator wc = new MidiCreator(filePath, iToneDataList);
      wc.createMidiFile(wp.getInstrumentIndex(), false);

      // Suggest installation location
      String path =
            iProp.getProperty(WhistlePropertyConstants.JMIDISHEETMUSIC_JAR);
      if (path == null)
      {
         iProp.setProperty(WhistlePropertyConstants.JMIDISHEETMUSIC_JAR,
               getJMSMPath());
      }

      RunDosUtil.runJar(WhistlePropertyConstants.JMIDISHEETMUSIC_JAR,
            "core.midi.sheet.music.JMidiSheetMusic", "\"" + filePath + "\"");
   }

   private static String getJMSMPath()
   {
      File root = new File(".");
      for (File subFile : root.listFiles())
      {
         if (subFile.isDirectory() && subFile.getName().startsWith(
               WhistlePropertyConstants.J_MIDI_SHEET_MUSIC_FOLDER_PREFIX))
         {
            for (File jar : subFile.listFiles())
            {
               if (jar.isFile()
                     && jar.getName().startsWith(
                           WhistlePropertyConstants.J_MIDI_SHEET_MUSIC_JAR_PREFIX)
                     && jar.getName().endsWith(".jar"))
               {
                  return subFile.getName() + File.separator + jar.getName();
               }
            }
         }
      }
      return null;
   }

   private void createRecordPage(JPanelExt panel)
   {
      btnRecord.setText("Record");
      btnRecord.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            recordactionPerform();
         }

      });
      panel.addComponentExt(btnRecord, iPanelMain.getCompExtWidth() - 194, 10,
            100, BUTTON_HEIGHT, CompExtEnum.MoveWithWidth);

      btnStopRecording.setText("Stop");
      GraphicsUtil.adjustFontForOS(btnStopRecording);
      btnStopRecording.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            if (iRecordMode == PLAY)
            {
               stopPlayingMidi(btnStopRecording);
               iRecordMode = NA;
            }

            if (iRecordMode == RECORD)
            {
               enableButtons(playList, true);
               btnStopRecording.setEnabled(false);
               captureThread.stopRecording();
               File file = new File(recordedfileName);

               // Give user a chance to change name
               String newName = JOptionPane.showInputDialog(null,
                     "Name of recording?", file.getName());

               if (!newName.toLowerCase().endsWith(".wav"))
               {
                  newName += ".wav";
               }

               if (!newName.equals(file.getName()))
               {
                  String newPath = WAVE_FOLDER + File.separator + newName;
                  File newfile = new File(newPath);
                  file.renameTo(newfile);
                  recordedfileName = newPath;
                  file = newfile;
               }
               refreshFileCombo(file.getName());
               iRecordMode = NA;
            }
         }

      });
      panel.addComponentExt(btnStopRecording, iPanelMain.getCompExtWidth() - 80,
            10, 70, BUTTON_HEIGHT, CompExtEnum.MoveWithWidth);

      iSampleDiagram = new SampleDiagram(panel.getCompExtWidth() - 20, 160);
      panel.addComponentExt(iSampleDiagram, 10, 50,
            panel.getCompExtWidth() - 20, 160, CompExtEnum.ResizeWithWidth);

      JTonePanel jTonePanel = new JTonePanel(panel.getCompExtWidth() - 20,
            panel.getCompExtHeight() - 210);
      iJToneList.add(jTonePanel);
      panel.addComponentExt(jTonePanel, 10, 210, panel.getCompExtWidth() - 20,
            panel.getCompExtHeight() - 210,
            CompExtEnum.ResizeWithWidthAndHeight);

   }

   private static List<ToneData> filterNotes(List<ToneData> toneDataList)
   {
      WaveProperties wp = WaveProperties.getInstance();
      for (ToneData toneData : toneDataList)
      {
         if (toneData.isTone())
         {
            // If the hertz is below limit
            if (toneData.getNote().getHertz() < wp.getMinHertz())
            {
               toneData.setIsNoTone();
            }

            // If tone length is below limit
            if (toneData.getNoteLength().getQuote() < NoteLength
                  .getQuote(wp.getNoteLength()))
            {
               toneData.setIsNoTone();
            }
         }
      }
      return toneDataList;
   }

   private void findTonesAction(final ChartPanel chartPanel2)
   {
      WaveData.showSlices(mDataset);
      WaveProperties wp = WaveProperties.getInstance();
      File file = new File(wp.getFileName());
      final JFreeChart chart = Charter.createChart(mDataset, file.getName());
      chartPanel2.setChart(chart);

      FrequenceFinder ff = new FrequenceFinder(WaveData.getDerives());

      iToneDataList = ff.createNoteList();
      iToneDataList = filterNotes(iToneDataList);
      if (debugMode() == DebugModeEnum.One)
      {
         iToneDataList = getDebugModeOneList();
      }
      initJTonePanels();
   }

   private static List<ToneData> getDebugModeOneList()
   {

      List<ToneData> lst = new ArrayList<ToneData>();

      lst.add(new ToneData("C2"));
      lst.add(new ToneData("C#2"));
      lst.add(new ToneData("D2"));
      lst.add(new ToneData("D#2"));
      lst.add(new ToneData("E2"));
      lst.add(new ToneData("F2"));
      lst.add(new ToneData("F#2"));
      lst.add(new ToneData("G2"));
      lst.add(new ToneData("G#2"));
      lst.add(new ToneData("A2"));
      lst.add(new ToneData("A#2"));
      lst.add(new ToneData("B2"));

      lst.add(new ToneData("C3"));
      lst.add(new ToneData("C#3"));
      lst.add(new ToneData("D3"));
      lst.add(new ToneData("D#3"));
      lst.add(new ToneData("E3"));
      lst.add(new ToneData("F3"));
      lst.add(new ToneData("F#3"));
      lst.add(new ToneData("G3"));
      lst.add(new ToneData("G#3"));
      lst.add(new ToneData("A3"));
      lst.add(new ToneData("A#3"));
      lst.add(new ToneData("B3"));

      lst.add(new ToneData("C4"));
      lst.add(new ToneData("C#4"));
      lst.add(new ToneData("D4"));
      lst.add(new ToneData("D#4"));
      lst.add(new ToneData("E4"));
      lst.add(new ToneData("F4"));
      lst.add(new ToneData("F#4"));
      lst.add(new ToneData("G4"));
      lst.add(new ToneData("G#4"));
      lst.add(new ToneData("A4"));
      lst.add(new ToneData("A#4"));
      lst.add(new ToneData("B4"));

      lst.add(new ToneData("C5"));
      lst.add(new ToneData("C#5"));
      lst.add(new ToneData("D5"));
      lst.add(new ToneData("D#5"));
      lst.add(new ToneData("E5"));
      lst.add(new ToneData("F5"));
      lst.add(new ToneData("F#5"));
      lst.add(new ToneData("G5"));
      lst.add(new ToneData("G#5"));
      lst.add(new ToneData("A5"));
      lst.add(new ToneData("A#5"));
      lst.add(new ToneData("B5"));

      lst.add(new ToneData("C6"));
      lst.add(new ToneData("C#6"));
      return lst;
   }

   private DebugModeEnum debugMode()
   {
      return iDebugMode;
   }

   private void initChart()
   {
      mDataset = new XYSeriesCollection();
      JFreeChart mChart = Charter.createChart(mDataset, "Wavfilename");
      chartPanel = new ChartPanel(mChart);
      chartPanel.setDomainZoomable(true);
   }

   private void initJTonePanels()
   {
      boolean firstPanel = true;
      for (JTonePanel jtonepanel : iJToneList)
      {
         jtonepanel.init(iToneDataList, firstPanel);
         firstPanel = false;
      }
   }

   private void initPlayStopButtons()
   {
      playList.add(btnPlayWave);
      playList.add(btnMidi);
      playList.add(btnNewWave);
      playList.add(btnRecord);

      stopList.add(btnStopWave);
      stopList.add(btnStopMidi);
      stopList.add(btnStopNewWave);
      stopList.add(btnStopMidi);
      stopList.add(btnStopRecording);
   }

   @Override
   public void mouseClicked(MouseEvent e)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void mouseDragged(MouseEvent e)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void mouseEntered(MouseEvent e)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void mouseExited(MouseEvent e)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void mouseMoved(MouseEvent e)
   {

   }

   @Override
   public void mousePressed(MouseEvent e)
   {
      if (e.getSource() instanceof JLabel)
      {
         showPage(buttonText.About);
      }
   }

   @Override
   public void mouseReleased(MouseEvent e)
   {
      // TODO Auto-generated method stub

   }

   private void playMidi(final int instrument, JButton stopButton)
   {
      enableButtons(playList, false);
      stopButton.setEnabled(true);

      WaveProperties wp = WaveProperties.getInstance();
      MidiCreator wc = new MidiCreator(TEMP_MIDI_FILE, iToneDataList);
      wc.createMidiFile(instrument, true);
      System.out.println("*** Tempo: " + wp.getTempo() + " **");
      midiPlayer = new MidiPlayer(TEMP_MIDI_FILE, wp.getTempo());
      for (JTonePanel jtonepanel : iJToneList)
      {
         midiPlayer.addActionListener(jtonepanel);
         jtonepanel.resetScrollBars();
      }
      midiPlayer.start();

      while (midiPlayer.getSequencer() == null)
      {
      }

      WaitForSong wait =
            new WaitForSong(midiPlayer.getSequencer(), playList, stopList);
      wait.start();
   }

   private void processFile(String filename)
   {
      XYSeriesCollection mWavedataset = WaveData.getWaveDataset(filename, null);

      if (mWavedataset == null)
      {
         JOptionPane.showMessageDialog(this, "Could not read file " + filename);
         return;
      }

      toneVolumeMax = WaveData.getMiddle() * toneVolumeMaxFactor;
      System.out.println("Middle factor: " + toneVolumeMax);

      File file = new File(filename);

      final JFreeChart chart =
            Charter.createChart(mWavedataset, file.getName());
      chartPanel.setChart(chart);

      findTonesAction(chartPanel);
   }

   private void recordactionPerform()
   {
      iRecordMode = RECORD;
      enableButtons(playList, false);
      btnStopRecording.setEnabled(true);
      iSampleDiagram.setTime(0.0);
      WaveProperties wp = WaveProperties.getInstance();
      recordedfileName = getNextFreeFileName(
            WAVE_FOLDER + File.separator + wp.getPrefix() + "1.wav");

      captureThread = new CaptureThread(recordedfileName);
      captureThread.addActionListener(this);
      captureThread.captureAudio();
   }

   private void refreshFileCombo()
   {
      iFileManager.refreshFileCombo(iCmbFilenames);
   }

   private void refreshFileCombo(String selectedItem)
   {

      refreshFileCombo();
      if (selectedItem != null)
      {
         iCmbFilenames.setSelectedItem(selectedItem);
         System.out.println("Setting filename to " + selectedItem);
      }
   }

   public final File saveFile(String path,
         javax.swing.filechooser.FileFilter fileFilter)
   {
      File file = new File(path);

      JFileChooser choose = new JFileChooser(file.getParent());
      if (fileFilter != null)
      {
         choose.addChoosableFileFilter(fileFilter);
      }

      choose.showSaveDialog(this);
      return choose.getSelectedFile();
   }

   private void showPage(buttonText textIndex)
   {
      if (textIndex == buttonText.Options)
      {
         String fileName = (String) iCmbFilenames.getSelectedItem();

         if (fileName == null)
         {
            JOptionPane.showMessageDialog(this, "No file selected");
         }
         else
         {
            OptionsFrame ofd = OptionsFrame.getInstance(fileName, this);
            ofd.setVisible(true);
         }
      }
      else
      {
         int index = textIndex.ordinal();

         for (int i = 0; i < iPages.size(); i++)
         {
            JPanelExt panel = iPages.get(i);
            if (i == index)
            {
               panel.setVisible(true);
               if (textIndex == buttonText.AutoConfig)
               {
                  runAutoConfig();
               }
               if (textIndex == buttonText.MidiPlayer)
               {
                  playMidiExternal();
               }
            }
            else
            {
               panel.setVisible(false);
            }
         }
         iPanelExt.updateComponentExt();
      }
   }

   public void runAutoConfig()
   {
      String wavfileName = (String) iCmbFilenames.getSelectedItem();
      AutoConfig ac = new AutoConfig(wavfileName, mDataset, this);
      Thread thread = new Thread(ac);
      thread.start();
   }

   private void stopPlayingMidi(JButton stopButton)
   {
      enableButtons(playList, true);
      stopButton.setEnabled(false);
      midiPlayer.stopPlaying();
   }

   static WhistleStudio getInstance()
   {
      if (iWhistleStudio == null)
      {
         iWhistleStudio = new WhistleStudio();
      }

      return iWhistleStudio;
   }

   public String getSelectedWave()
   {
      return (String) iCmbFilenames.getSelectedItem();
   }

   @Override
   public JScorePanel getScorePanel()
   {
      return jScorePanel;
   }

   @Override
   public void autoConfig()
   {
      showPage(buttonText.AutoConfig);

   }

}