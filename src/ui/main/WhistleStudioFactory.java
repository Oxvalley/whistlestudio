package ui.main;

import common.properties.SettingProperties;

public class WhistleStudioFactory
{
   public static WhistleStudio getWhistleStudioInstance()
   {
      return WhistleStudio.getInstance();
   }

   public static SettingProperties getSettingPropertiesInstance()
   {
      return SettingProperties.getInstance();
   }

   
}
