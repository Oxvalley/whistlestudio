package utils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import common.utils.AppInfoInterface;
import common.utils.FileHelper;
import common.utils.LogUtil;
import common.utils.PackInstallationUtil;
import system.AppInfo;
import ui.main.WhistlePropertyConstants;

public class PackInstallationUtilWS extends PackInstallationUtil
{

   private static String[] files = new String[] { "rec04.wav", "rec31.wav",
      "tape-a-talk.wav", "vissel1.wav", "vissel2.wav", "v�gsk�l-vissel1.wav",
      "rec15.wav", "rec11.wav" };

   private static List<String> filesList = Arrays.asList(files);

   public PackInstallationUtilWS(String appName, String version,
         String jdkVersion, String appNameVersion, String mainClassNAme,
         boolean packSource)
   {
      super(appName, version, jdkVersion, appNameVersion, mainClassNAme,
            packSource);
   }

   public static void main(String[] args)
   {
      boolean packSource = false;
      if (args.length > 0)
      {
         if (args[0].equals("src"))
         {
            packSource = true;
         }
      }
      
      AppInfoInterface appInfo = AppInfo.getInstance();
      String name = appInfo.getApplicationName();
      String version = appInfo.getVersion();
      String jdkVersion = appInfo.getJdkVersion();
      String appNameVersion = "WhistleStudio_v_" + getVersionNoDots(version);
      String mainClass = "ui.main.WhistleStudio";
      PackInstallationUtil pi = new PackInstallationUtilWS(name, version,
            jdkVersion, appNameVersion, mainClass, packSource);
      pi.execute();
   }

   @Override
   protected void copyFilesAndFolders()
   {
      super.copyFilesAndFolders();
      copyFolderAndLog("images");
      createFolderAndLog("midi");
      createFolderAndLog("mp3");
      copyWavesFolder();
      createFolderAndLog("waves/new");
      copyLibFolder("libs");
      copyFileAndLog("whistle.png");
      copyFileAndLog("readme.txt");
      copyJMidiSheetMusicFolder();
   }

   private void copyJMidiSheetMusicFolder()
   {
      File root = new File(".");
      for (File subFile : root.listFiles())
      {
         if (subFile.isDirectory() && subFile.getName().startsWith(
               WhistlePropertyConstants.J_MIDI_SHEET_MUSIC_FOLDER_PREFIX))
         {
            copyFolderAndLog(subFile.getName());
            return;
         }
      }

      System.out.println("No installation of JMidiSheetMusic found");
   }

   protected void copyWavesFolder()
   {
      File dir = new File("waves");
      File destDir = new File(iFolderName + "/waves");
      destDir.mkdir();

      if (dir.isDirectory())
      {
         File[] subDirs = dir.listFiles();
         for (File subDir : subDirs)
         {
            if (isallowedName(subDir))
            {
               if (subDir.isDirectory())
               {
                  FileHelper.copyFoldertoFolder(subDir.getAbsolutePath(),
                        iFolderName + "/waves");
               }
               else
               {
                  try
                  {
                     FileHelper.copy(subDir.getAbsolutePath(),
                           iFolderName + "/waves", true);
                  }
                  catch (IOException e)
                  {
                     LogUtil.print(
                           "Could not copy file " + subDir.getAbsolutePath());
                  }
               }
            }
         }
      }

   }

   protected static boolean isallowedName(File subDir)
   {
      String fileName = subDir.getName().toLowerCase();

      if (subDir.isDirectory())
      {
         return false;
      }
      else
      {
         return (filesList.contains(fileName));
      }
   }

   @Override
   protected List<String> getProjectDependendies()
   {
      List<String> list = super.getProjectDependendies();
      list.add("../../Common Project");
      return list;
   }

}
